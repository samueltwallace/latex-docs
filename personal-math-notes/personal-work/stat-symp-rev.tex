
\documentclass{article}

\usepackage{chapterbib}
\usepackage{amsmath}                                                               
\usepackage{amssymb}                                                               
\usepackage{hyperref}
\usepackage{mathrsfs}                                                              
\usepackage{mathtools}                                                             
\usepackage{amsthm}
\newtheorem{thm}{Theorem}                                                          
\newtheorem{defn}{Definition}                                                      
\newtheorem{prop}{Proposition}                                                     
\newtheorem{rmk}{Remark}                                                           
\newtheorem{cor}{Corollary}                                                        
\newtheorem*{pf}{Proof}
\newtheorem{lem}{Lemma}

\title{The Liouville Equation and Hamiltonian Dynamics}
\author{Samuel Wallace}
\date{\today}



\begin{document}   


\maketitle


\begin{abstract}

	Here we draw on equations from statistical mechanics to investigate Hamiltonian dynamics. We show how a PDE can reveal periodic orbits of a Hamiltonian system, and give an idea to when the converse may hold. We additionally explore the equation as a Hamiltonian flow on a Lie group, specfically, the Lie group of Hamiltonian isotopies, and use the results of the previous section to show that there can be a correspondance between periodic orbits of a Hamiltonian systems and the induced Hamiltonian system on the infinite-dimensional Lie group.

\end{abstract}

\tableofcontents

\section{Introduction}

\indent Symplectic geometry has seen development as a generalization of Hamiltonian dynamics on a manifold. Inspiration from physics has driven research in development of symplectic geometry, notably, in quantization theories. However, there is one aspect of classical physics that has not been explored: classical statistical mechanics. In this paper, we intend to show how classical statistical mechanics can be used to reveal aspects of Hamiltonian dynamics that may not have been apparent beforehand.\\
\indent It may seem at first glance that using \textit{statistical} mechanics to reveal properties of \textit{individual} orbits would not produce results. However, we will use a form of deterministic, rather than probabilistic, statistical mechanics. This comes in the form of the microcanonical ensemble. In physics, the microcanonical ensemble is the evolution of a collection of systems with the same equations of motion that do not interact. To put it more explicitly, imagine we have set up many simple pendulums set up that do not interact. We record the initial conditions of each pendulum. We then let the pendulums swing, and record their locations in phase space at some later time $t$. We then write down a function that describes the number of pendulums in each state at time $t$. Clearly, since the systems do not interact, we should expect that the same number of systems are in the same state along trajetories of the equations of motion. Explicitly, if we have $ f: P \times \mathbb{R} \to \mathbb{R}$, where $P$ is the relevant phase space, describing the number of systems at $x \in P$ and at time $t \in \mathbb{R}$ by the number $f(x,t)$, then it should follow the evolution equation:

\begin{equation}
	f(x,t) = f(Fl_{X}^{-t} (x))
	\label{eqn:lie}
\end{equation}

Where $X$ is the vector field on phase space describing the equations of motion. This is easy to find determine if the equations of motion are exactly solvable; however, this is the not case in interesting examples. We can simplify this by taking a derivative at $t=0$ and get the following differential equation:
\[
	\frac{\partial f}{\partial t} =\mathcal{L}_X f
\]
Where $\mathcal{L}_X$ is the Lie derivative with respect to the vector field $X$ (note that depending on your sign convention, a negative Lie derivative may be correct). Now lets assume $P$ is a symplectic manifold and $X=X_H$, the Hamiltonian vector field of some function $ H: P \to \mathbb{R}$. Then we can write equation \ref{eqn:lie} as:
\[
	\frac{\partial f}{\partial t} = \mathcal{L}_{X_H} f = \{ H,f \} 
\]

\indent And this is called the \textbf{Liouvile Equation}. This is the main equation of study of this paper.

\section{The Liouville Equation and Periodic Orbits}


\indent The main equation we will study is the Liouville equation:


\begin{defn}

	Let $(M, \omega)$ be a symplectic manifold, and let $ \{ , \} $ be the Poisson bracket arising from the symplectic form. Let $ H: M \to \mathbb{R}$ be a smooth function. Then the \textbf{Liouville equation} is the PDE given by

\begin{equation}
\frac{\partial f}{\partial t} = \{ H,f \} 
\label{eqn:lville}
\end{equation}
At this point, we do not specify the function space, or generalized function space, that $f$ may lie in, though we do note that it is a function (or generalized function) with domain of some subset of $M \times \mathbb{R}$. We do note that we will be interested in the Cauchy problem for this PDE, i.e., specify a intial distribution $f(x,0) = u(x)$ and look for solutions to the PDE satisfying this condition.

\end{defn}

Note the Liouville equation can also be stated on a Poisson manifold; we do not need the full strength of a symplectic structure. We would now like to examine properties of solutions of the Liouville equation. We will start with the following observations:



\begin{rmk}

	The map $f \mapsto \{ H,f \} $ is sometimes skew adjoint operator in some subset of $C^1(M) \cap L^2(M, \mathbb{C}; \mu)$, where $\mu$ is some measure. By 'sometimes,' we mean if certain conditions are put on the function $H$. The skew-adjointness is seen in a symplectic chart by the following manipulation, when valid:
\[
	\int_U \overline{g} \{H,f\} d\mu
\]	
\[= \int_U \overline{g} \sum_i \left( \frac{\partial H}{\partial p_i} \frac{\partial f}{\partial q_i} - \frac{\partial H}{\partial q_i} \frac{\partial f}{\partial p_i} \right) d\mu = \sum_i \left( \int_U \overline{g} \frac{\partial H}{\partial p_i} \frac{\partial f}{\partial q_i} - \overline{g} \frac{\partial H}{\partial q_i} \frac{\partial f}{\partial p_i} d\mu \right)
\]
\[
	= \sum_i \left( \int_U f \frac{\partial H}{\partial q_i} \frac{\partial \overline{g}}{\partial p_i} - f \frac{\partial H}{\partial p_i} \frac{\partial \overline{g}}{\partial q_i} d\mu \right) = \int_U \sum_i f \left( \frac{\partial H}{\partial q_i} \frac{\partial \overline{g}}{\partial p_i} - \frac{\partial H}{\partial p_i} \frac{\partial \overline{g}}{\partial q_i} \right) 
\]
	\[= \int_U \{ H, \overline{g} \} f d\mu
\]
\indent We can make several comments on necessary conditions must be met for these manipulations to be valid. First, we have to get rid of the 'boundary terms;' the extra terms that arise as we change the order of integration and receive contributions from the values of $f,\overline{g}$ on $\partial U$. We also have to deal with the terms $ \frac{\partial^2 H}{\partial q_i \partial p_i}$ when transferring the derivative from $f$ to $\overline{g}$. We will address sufficient conditions for these manipulations to be valid in \textbf{?????} % TODO: ADD APPENDIX FOR SUFFICIENT CONDITIONS??
\indent Also notice that the measure we use for the inner product need not be the Liouville measure. We chose to do so here for later applications, when we would like to make sympectic transformations on $M$ without disturbing the inner product.

\end{rmk}


Now we will address the main building blocks in relating the Lioville equation to the dynamics of the original Hamiltonian system.


\begin{lem}

	Suppose $L_H: A \subset C^1(M) \cap L^2(M, \mathbb{C}; \mu) \to L^2(M, \mathbb{C}; \mu); \hspace{4pt} f \mapsto \{ H,f \} $ is skew adjoint. Then $L_H$ has eigenvalues along the imaginary axis.

\end{lem}


\begin{pf}

If $L_H$ is skew adjoint, then $i L_H$ is self-adjoint, and hence has all real eigenvalues. Thus $L_H$ has only purely imaginary eigenvalues.

\end{pf}


\begin{lem}

	Suppose $L_H$ is skew adjoint on $A \subset C^1(M, \mathbb{C}) \cap L^2(M, \mathbb{C}; \mu)$. If the path of operators $U_t = e^{L_H t}$ is defined for $t \in [0,T]$, then the solution to the Cauchy problem for the Liouville equation with initial data $\phi: M \to \mathbb{C}$ is given by 

\[
	f(x,t) = e^{L_H t} \phi(x)
\]
\label{eqn:exp}
\end{lem}


\begin{pf}

The equation follows from a simple differentiation:
\[
	\frac{\partial f}{\partial t} = L_H e^{L_H t} \phi(x) = L_H f(x,t) = \{ H,f \} 
\]


\end{pf}




	Comparing the solution given in Lemma \ref{eqn:exp} to Equation \ref{eqn:lie}, we are tempted to say that $e^{L_H t} = Fl_{X_H}^t$. We will not directly prove this, but in some restricted cases it may be true. 





\begin{lem}

	Suppose $i \lambda$ lies in the point spectrum of $ L_H: A \to L^2 (M, \mathbb{C}; \mu)$, which is to say $L_H$ has an eigenvector in $L^2$: $L_H \phi = i \lambda \phi$. Suppose additionally that $e^{L_H t}$ is defined over the interval $t \in [0, T]$ with $T > 2 \pi/ \lambda$. Then the solution of the Cauchy problem of \ref{eqn:lville} with initial data $\phi$ is periodic is periodic with period $2 \pi/ \lambda$.
\end{lem}


\begin{pf}

Let $\Phi$ be the solution to the Cauchy problem with initial data $\phi$. Then 
\[
	\Phi(x,2 \pi n / \lambda) = e^{L_H 2 \pi n / \lambda} \phi(x) = e^{ (i \lambda) 2 \pi n / \lambda)} \phi(x) = e^{2 \pi i n} \phi(x) = \phi(x)
\]
So $\Phi$ is periodic in $t$ with period $2 \pi/\lambda$.

\end{pf}






\indent We can extend the results of the previous lemma by considering larger function spaces and looking at the spectrum of operator $L_H$ on these function spaces. The crucial theorem for guarantee of existence of $e^{L_H t}$ is Hille-Yosida. Beyond Banach spaces, we must be careful of interpretation of the existence of the 'time-evolution operator' $e^{L_H t}$.





Now we get into the main results of this section. It relates spectral properties of the operator $L_H$ to the dynamics of $X_H$.



\begin{thm}

	Suppose $X_H$, the Hamiltonian vector field correspoding to the Hamiltonian function $H$,  has a periodic orbit with period $2 \pi / \lambda$. Then $i \lambda$ is an eigenvalue of $L_H$, as an operator on $\mathcal{D}'(M)$. Thus, periodic orbits of $X_H$ lift to periodic solutions of the Liouville equation. 

\end{thm}



\begin{pf}

	Suppose $Fl_{X_H}$ has a periodic orbit starting at a point $x_0 \in M$, so that $Fl_{X_H}^{2 \pi / \lambda} (x_0) = x_0$. Let $x(t) = Fl_{X_H}^t (x_0)$ be the orbit; it is naturally a solution to the equations of motion. Let $C = x([0, 2 \pi/ \lambda])$, the periodic orbit of the system, and let $\delta_C$ be the Dirac distribution on the orbit. As a closed 1-manifold, there is a diffeomorphism $ \phi: C \to S^1$. Now $ v=\phi_* X_H $ is a non-zero vector field on $S^1$; we can make it a constant vector field by the diffeomorphism $ \psi: S^1 \to S^1; \hspace{4pt} \theta \mapsto 2 \pi ( \int_0^{2 \pi} \frac{d\varphi}{v(\varphi)})^{-1} \int_0^{\theta} \frac{d \varphi}{v(\varphi)}$. Set $\alpha = \psi \circ \phi$, so that $v_0 = \alpha_* X_H$ is a constant vector field on $S^1$. \\
\indent Now notice that the Liouville equation, when restricticted to $C$ and pushed forward through $\alpha$, takes the form
\[
	\frac{\partial f}{\partial t} = \mathcal{L}_{\alpha_* X_H} \alpha_*f
\]
So that if $f: S^1 \to \mathbb{C}$ satisfies the pushed-forward Liouville equation, the naturality of the Lie derivative will allow us to pull the solution back to $C$ to a solution of the original Liouville equation. But with a constant vector field, the equation becomes simple to solve:
\[
\frac{\partial f}{\partial t} = v_0 \frac{\partial f}{\partial \theta}
\]
Take $f(\theta, t) = e^{i( \theta - \lambda t)}$. This clearly satisfies the Liouville equation pushed forward to $S^1$, but also satifies the periodicity condition $f(\theta, 2 \pi / \lambda) = f(\theta,0)$ (Notice also this implies that $v_0 = \lambda^{-1}$). Now we can push back the function to $C$, and multiply it by $\delta_C$ to get a solution to the Liouville equation on $M$. Now we would like to show that this is an eigenvalue of $L_H$. But this is easy to see, as
\[
	L_Hf = \frac{\partial f}{\partial t} = \frac{\partial (\alpha_* f)}{\partial t} = i \lambda f
\]
So $i \lambda$ is a genuine eigenvalue of $L_H$ acting on $\mathcal{D}'(M)$.

\end{pf}

This theorem is important because it 'lifts' dynamics of the Hamiltonian orbits to dynamics of the solutions to the Liouville equation. The following remark shows that it is plausible, and sometimes possible, to go the other way; i.e. descend time-periodic solutions to the Liouville equation to dynamics of the Hamiltonian system.


\begin{rmk}

Time-periodic solutions to the Liouville equation can correspond to periodic orbits of the Hamiltonian system.

\end{rmk}


\begin{pf}

	Suppose $e^{L_Ht} = Fl_{X_H}^t$ and $\phi$ is an eigenvector of $L_H$ is some appropriate (generalized) function space with eigenvalue $i \lambda$. Then we know that
\[
	(\phi \circ Fl_{X_H}^{2 \pi / \lambda} )(x) = \phi(x)
\]
If a left-inverse of $\phi$ exists over some suitable conditions (restriction of domain, for example), then we may conclude that
\[
	Fl_{X_H}^{2 \pi / \lambda}(x) = x
\]


\end{pf}

Although the details need to be sorted out, it is clear that studying the Liouville equations as a PDE, we can derive information about the Hamiltonian dynamics. A more broad and general question might be raised about the Liouville equation: What do spectral properties of the Poisson bracket operator tell us about the dynamical properties of the Hamiltonian system? Or even more broad, what do properties of the Liouville equation tell us about the Hamiltonian dynamics?


\section{The Liouville Equation as Euler-Arnold Equations}

We will start examining the Liouville equation as a \textit{Euler-Arnold equation}, which is to say, the flow of a Hamiltonian on a Lie group. The Lie group we will be working on is infinite-dimensional, and there are many intricacies of dealing with such groups. We will follow the presentation of Fr\'echet Lie groups, Lie groups whose coordinate charts take values in a Fr\'echet space and whose transition maps are G\^ateaux differentiable, from \cite{khesin2009}. We will start by forming the abstract Lie algebra relevant to the Liouville Equation.



\begin{defn}

	Let $(M, \omega)$ be a compact symplectic manifold. Then let $ \mathfrak{g} = C^{\infty}(M)$, with the seminorms $\Vert \cdot \Vert_k = \sup_M \vert \partial^{\alpha}f \vert, \hspace{4pt} \vert \alpha \vert = k$. These seminorms make $ \mathfrak{g}$ into a Fr\'echet space where integration and differentiation are continuous operations. Moreover, the Lie bracket $ \{ , \} $ induced by the symplectic form $\omega$ makes $ \mathfrak{g}$ an abstract Lie algebra. We will define $ \mathfrak{g}$ as the abstract Lie algebra with this structure; we will call it the \textbf{Lie algebra of Hamiltonians}.

\end{defn}

We will start writing elements of $ \mathfrak{g}$ as lowercase Latin letters; e.g., $h \in \mathfrak{g}$ a Hamiltonian. \\
\indent The corresponding Lie group of this Lie algebra is the group of Hamiltonian isotopies, ambient isotopies that are generated by Hamiltonian vector fields. The exact correspondence between Lie group and algebra is given by the following definition: 


\begin{defn}

	Let $\exp: \mathfrak{g} \to \mathrm{Ham}(M)$, where $ \mathrm{Ham}(M)$ is the group of Hamiltonian isotopies of $M$, by the 'time-one' map of the flow. More formally, let $h \in \mathfrak{g}$, $X_h$ be its associated Hamiltonian vector field, and $Fl_{X_h}^t$ be the flow of $X_h$. Then we define $\exp{h} = Fl_{X_h}^1$, the time-one map for the flow. Note that this map is automatically surjective (by definition of $ \mathrm{Ham}(M)$).	

\end{defn}

There is some care needed in defining the composition of two maps in $ \mathrm{Ham}(M)$, because the concatenation of two smooth isotopies is not necessarily smooth. Instead, we define a 'smooth concatenation,' as presented in \cite{hirsch1976}. Let $ \tau: [0,1] \to [0,1]$ be a $C^{\infty}$ map that takes a neighborhood of 0 to 0, and a neighborhood of 1 to 1. Then for two Hamiltonian isotopies $\phi_t, \psi_t$ the concatenation 
\[
	(\phi * \psi)_t = \begin{cases}
		\phi(\tau(2t)) &   t < 1/2 \\
	\psi(\tau(2t-1)) &  t \geq 1/2  
	\end{cases}
\]
is smooth and forms the corresponding Lie group. We will not be as concerned with the structure of the group $ \mathrm{Ham}(M)$, but will be more concerned with its properties as a Lie group (its local behavior near the identity).\\
\indent Now we will get to the main idea of this section. We can impose a Poisson structure on $C^{\infty} ( \mathfrak{g})$ with the \textit{Lie-Poisson} bracket, and viewed this way, the Liouville equation becomes the flow of a Hamiltonian function on the Lie group. The following assertions will make this clear.



\begin{defn}

	We define the dual space of $ \mathfrak{g}$, $ \mathfrak{g}^*$, as $C^{\infty}(M)$, with the pairing for $h \in \mathfrak{g}, \xi \in \mathfrak{g}^*$ as $ \langle \xi, h \rangle = \int \xi (x) h(x) d\mu(x)$. 

\end{defn}

Notice this dual space is a subset of the usual continuous dual space of $ \mathfrak{g}$, which is the classical space of distributions $ \mathcal{D}'(M)$. Though it is tempting to take the space of distributions as the dual space, this can cause issues, especially when we need to consider the double dual $ \mathfrak{g}^{**}$. We will point out what can go wrong as it comes up. It would be interesting to see how the dual space can be extended in a way that will avoid the problems that will be pointed out.

\begin{defn}

We define the \textit{Lie-Poisson bracket} over $h \in \mathfrak{g}^*$ as the map 
\[
	\{ , \}_{h}: C^{\infty}( \mathfrak{g}^*) \times C^{\infty}( \mathfrak{g}^*) \to C^{\infty}( \mathfrak{g}^*); \hspace{4pt} (F,G) \mapsto \{ F,G \}_{h} =  \langle h, \{ dF, dG \}  \rangle 
\]


\end{defn}


Note that there some would be some problems with this definition had we used the full space of distributions: namely, that $dF: \mathfrak{g}^{**} \to \mathbb{R}$, and since $ \mathfrak{g}$ may not be equal to $ \mathfrak{g}^{**}$, the Lie bracket $ \{ dF, dG \}$ may not be defined! Now we will use the following result from \cite{khesin2009}:


\begin{thm}

	For the Lie-Poisson bracket over $h \in \mathfrak{g}^*$, the flow of a Hamiltonian function $H: \mathfrak{g}^* \to \mathbb{R}$ the Hamiltonian flow is given by:
\[
	\dot{f} = - \mathrm{ad}^*_{dH_f} h
\]

\label{thm:eul-arn}
\end{thm}

Now we would like to see the Liouville equation as a this type of equation, but we still have some building to do. As is the case with Euler-Arnold equations, this invariant manifolds of the dynamical system is the coadjoint orbits of $h$. We can make these coadjoint orbits into symplectic manifold by the following symplectic form, modified from \cite{audin2004}:


\begin{defn}

	Let $ \mathcal{O}(h) \subset \mathfrak{g}^*$ be the coadjoint orbit of $h \in \mathfrak{g}^*$. Then we define $\omega_h$ a non-degenerate two-form on $ \mathcal{O}(h)$ (note that $T_x \mathcal{O}(h) \approx \mathrm{ad}_{ \mathfrak{g} }^* h$) by
\[
	\omega_h ( \mathrm{ad}_{\xi}^* h, \mathrm{ad}_{\chi}^* h) = \langle h, \{ \xi, \chi \}  \rangle 
\]


\label{def:symp-form}
\end{defn}

Now we relate this symplectic form to the Lie-Poisson bracket:


\begin{lem}

	The form $\omega_h$ generates the same Hamiltonian dynamics on $ \mathcal{O}(h)$ as the Lie-Poisson bracket over $h$.
\label{lem:flow}
\end{lem}


\begin{pf}

This is easy to check:
\[
	\langle h, \{ dF, dG \}  \rangle = \omega_h ( \mathrm{ad}_{dF}^* h, \mathrm{ad}_{dG}^* h) 
\]

Where the left side is the original Lie-Poisson bracket and the right side is $\omega_h (\xi_F, \xi_G)$.
\end{pf}

Now we would like to express the Liouville equation as a Hamiltonian flow, and extend that to a geodesic flow. We define an inner product first:


\begin{thm}

	Let $\mu$ be a measure on $M$ such that $L_h$ is skew-symmetric. Then $ \left( , \right)_{\mu}$ is an inner product on $ \mathfrak{g}^*$ for which the operator $ \mathrm{ad}_h^*$ is skew-symmetric defined by 
\[
	\left( \xi,\chi \right)_{\mu} = \int \xi (x) \chi (x) d\mu 
\]

\end{thm}
Note that an adjoint action being antisymmetric with respect to an inner product is equivalent to it being $G$-invariant (where $G$ acts on $ \mathfrak{g}^*$ by coadjoint action).

\begin{pf}

Clearly
\[
	\left( \xi, \mathrm{ad}_h^* \chi \right)_{\mu} = \int_M \xi (x) L_h \chi(x) d\mu
\]
So skew-symmetry of $L_h$ is the same as skew-symmetry of $ \mathrm{ad}_h^*$.

\end{pf}


Notice the pairing between $ \mathfrak{g}^*$ and $ \mathfrak{g}$ depends on the measure $\mu$ as well. Now we are ready to start back with the Liouville equation:


\begin{thm}

	Let $\mu$ be a measure. The Hamiltonian function $H: \mathfrak{g}^* \to \mathbb{R}; \hspace{4pt} \xi \mapsto \left( \xi,\xi \right)_{\mu}$ satisfies $ dH_{\xi} = \xi \in \mathfrak{g}$.
\label{thm:deriv}
\end{thm}



\begin{pf}
\[
	\frac{d}{dt}H(\xi + t \chi) \restriction_{t=0} = \frac{d}{dt}\int_M \left( \xi + t \chi \right)^2 d\mu \restriction_{t=0} = \int_M \xi \chi d\mu = \langle \xi, \chi \rangle 
\]
So $dH_{\xi}(\chi) = \langle \xi, \chi \rangle$. 
\end{pf}

Now we can make a complete statement about the Liouville equation.



\begin{thm}

	Let $\mu$ be a measure on $M$ such that $L_h$ is skew-symmetric. Then the Liouville equation is the flow of the Hamiltonian given in Theorem \ref{thm:deriv} on the coadjoint orbits of $H$, with the symplectic form on the orbit given by Definition \ref{def:symp-form}. 

\end{thm}


\begin{pf}

	It follows almost immediately. From Lemma \ref{lem:flow}, we have that $H$ generates the flow given by Theorem \ref{thm:eul-arn}. But by Theorem \ref{thm:deriv}, this is 
\[
	\dot{f} = - \mathrm{ad}_{dH_f}^* h = - \mathrm{ad}_f^* h = - \{ f,h \} = \{ h,f \}  
\]


\end{pf}

Now we can interpret this inner product on $ \mathfrak{g}$, and as noted before, the skew-symmetry of the adjoint operator makes a $G$-invariant metric. 






\appendix

\section{Conditions for Skew-symmetry}

\indent Here we will try to give some sufficient conditions for when the operator $L_H$ is skew-symmetric. The guiding idea is that

\[
\langle g,L_H f \rangle + \langle L_Hg, f \rangle = \int_M \{ H, fg \} d\mu
\]
So that finding measures for Hamiltonians that make the Poisson bracket with a product vanish is the same as finding a measure such that $L_H$ is skew-symmetric.


\begin{prop}

	Let $M$ be a closed manifold, $H$ a Hamiltonian function. If the smooth measure $\mu$ satisfies the property that the map $ m: C^{\infty}(M) \to \Lambda^n(M) \hspace{4pt} f \mapsto \mu \cdot i_{X_H} df$ has image contained in the set of exact forms; i.e., $df(X_H) \mu = d \Omega$ for any function $f$ and some form $\Omega \in \Lambda^{n-1}(M)$.

\end{prop}


\begin{pf}
	\[
		\langle g, L_H f \rangle + \langle L_H g, f \rangle = \int_M \{ H, fg \} d\mu = \int_M d(fg) (X_H) \mu = \int_M d\Omega = \int_{\partial M} \Omega = 0 
	\]
\indent Since $\partial M = \varnothing$.
\end{pf}







\bibliographystyle{unsrt}
\bibliography{stat-symp}


\end{document}

