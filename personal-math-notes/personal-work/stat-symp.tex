\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{chapterbib}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{amsthm}
\newtheorem{prop}{Proposition}
\newtheorem*{pf}{Proof}

\title{Statistical Mechanics and Symplectic Geometry}
\author{Samuel Wallace}

\begin{document}

\maketitle

\section{Introduction}
\indent Symplectic geometry is one generalization of physics with many aspects. One aspect that has seemingly not been explored is ideas of classical statistical mechanics applied to symplectic geometry. At the outset, it might seem that statistical mechanics might tell us nothing new about a symplectic manifold, as it works on all the existing dynamics and structure and lifts to a probabilistic system, seemingly 'losing' information about the Hamiltonian dynamics or symplectic form or other symplectic data. However, we will try to convince you that the probabilistic systems can reveal properties of the symplectic data that was not apparent beforehand. \\
\indent We will explore the different 'ensembles' of statistical mechanics and see what each one can tell us about symplectic data. We start with the microcanonical ensemble, which describes the eventual fate of a fixed number of identical, non-interacting systems under the same Hamiltonian. Then we will move to the canonical ensemble, which describes a single system under probabilistic perturbations. Then we will consider the Grand canonical ensemble, which describes a collection of particles free to leave the system and be perturbed.

\section{Microcanonical Ensemble}

\subsection{The Liouville Equation}

\indent The microcanonical ensemble in physics starts with the idea of \textit{non-interacting identical subsystems}. The physical set up might go something like this: say we have some mechanical system, and we set up many copies of the system in such a way that they don't interact. We record the initial conditions of each copy, and then let the systems evolve in time according to their equations of motion. Then we record the state of each copy at a later time, and compare to the original distribution of initial conditions. \\
\indent Mathematically, we can describe the microcanonical ensemble as follows: we define a function on symplectic manifold $(M,\omega)$ representing the distribution of copies in phase space. We then demand that all the particles evolve according to some Hamiltonian dynamics of a Hamiltonian function $H: M \to \mathbb{R}$ and don't interact. If $f: M \times \mathbb{R} \to \mathbb{R}$ is the distribution of particles, we can say that the distribution follows the time evolution equation:
\begin{equation}
    \label{eqn:ham-flow}
    f(x,t+\tau) = f(\mathrm{Fl}_{X_H}^{-\tau} (x), t)
\end{equation}

\indent Where $\mathrm{Fl}_{X_H}^{-\tau}$ is the flow of the Hamiltonian vector field corresponding to $H$. This tells us the along a trajectory of the Hamiltonian system is conserved. This formulation of microcanonical ensemble requires exactly knowing the formula for the Hamiltonian flow, which is often not an easy task. Instead, we will show that by taking a derivative of \ref{eqn:ham-flow} at $\tau=0$, we get the \textbf{Liouville Equation}:
\begin{equation}
    \label{eqn:liouville}
    \frac{\partial f}{\partial t} = \{H,f\}
\end{equation}

\indent Where we have chosen some sign convention for the Poisson bracket associated with the symplectic structure of $M$. Clearly every solution of \ref{eqn:ham-flow} is a solution to \ref{eqn:liouville} because
\[ \frac{d}{d\tau}f(\mathrm{Fl}_{X_H}^{-\tau} (x), t) = \mathcal{L}_{X_H} f(x,t) = \pm \{H,f\} \]
\indent And of course, the ordering of the bracket is up to convention. An alternative description of the Liouville equation can be formulated in terms of the contact manifold $(\tilde{M}, \tilde{\omega})$, where $\tilde{M} = M \times \mathbb{R}$ and $\tilde{\omega}$ is the canonical contact form. It's an exercise in \cite{abraham1978} to show for that the suspended flow of $X_H$ in $\tilde{M}$, $\tilde{X}_H$, 
\[ \mathcal{L}_{\tilde{X}_H}f = \frac{\partial f}{\partial t} + \{f, H\}  \]
So we can think of the Liouville equation as expressing the fact that $f$ is conserved along trajectories of the suspended flow.

\subsection{Periodic Orbits}
\indent For a Hamiltonian function $H: M \to \mathbb{R}$, we have an associate Liouville Equation for its dynamics. We would now like to show how the Liouville equation can reveal details about the Hamiltonian dynamics. The first step is to think about solutions to the Liouville equation. We do this by (formally) separating variables in the Liouville Equation. \\
\indent Suppose $f = T(t)X(x); \hspace{4pt} T: \mathbb{R} \to \mathbb{R}, \hspace{2pt} X: M \to \mathbb{R}$. Then the Liouville Equation can be rearranged to read:
\[ \frac{T'}{T} = \frac{\{ H, X \}}{X} = \lambda \]
\indent Where $\lambda$ is a constant. Thus separation of variables reduces to finding eigenvalues of the Poisson Bracket with $H$. \\
\indent To find eigenvalue of the Poisson bracket, we notice that the operator $L_H$ whose action is given by $L_H f = i\{H,f\}$ is (formally) symmetric in some appropriate subset of $L^2(M, \mathbb{C}; \Omega)$, where $\Omega$ is the Liouville volume form. If we work in symplectic coordinates $(p(x),q(x),U)$, we must make the additional assumption that $\frac{\partial^2 H}{\partial q \partial p}=0$, so that
\[ \langle g, L_H f \rangle = i \int_U \overline{g} \{H,f\} \Omega = i \int_U \overline{g} \sum \left( \frac{\partial H}{\partial p} \frac{\partial f}{\partial q} - \frac{\partial H}{\partial q} \frac{\partial f}{\partial p} \right) \Omega\]
\[= i \int_U \sum \left( \frac{\partial H}{\partial q}\frac{\partial \overline{g}}{\partial p} - \frac{\partial H}{\partial p} \frac{\partial \overline{g}}{\partial q} \right)f \Omega = \int_U \overline{i \sum \left( \frac{\partial H}{\partial p} \frac{\partial g}{\partial q} - \frac{\partial H}{\partial q}\frac{\partial g}{\partial p} \right)}f \Omega = \langle L_H g, f \rangle\]

\indent So $L_H$ has real eigenvalues, which in turn means $\{H,\cdot\}$ has eigenvalues along the imaginary axis. Looking back at the Liouville Equation, we can write down an equation resembling the Schr\"odinger equation:
\begin{equation}
    \label{eqn:c-liouville}
    i \frac{\partial f}{\partial t} = L_H f
\end{equation}

For complex valued functions $f$ on $M \times \mathbb{R}$. We can look for solutions or weak solution to the Cauchy problem of Equation \ref{eqn:c-liouville} in some appropriate function or generalized function space. Thinking of separation of variables again, we propose the following:


\begin{prop}
    Suppose in some appropriate function space (or generalized function space), the complex valued (generalized) function $\phi$ with domain $M$ is an eigenvalue for $L_H$ with real eigenvalue $\omega$. Then (formal) solution to the Cauchy problem with initial data $\phi$ is periodic with frequency $\omega$.
\end{prop}

\begin{pf}
    (Sketch of Proof) The formal solution to equation \ref{eqn:c-liouville} is given by 
    \[ \Phi(x,t) = e^{-iL_H t} \phi(x) \]
    (In a Banach space, we can use Hille-Yosida or similar). Then since $\phi$ is an eigenvector, we can write
    \[ e^{-i L_H t} \phi = e^{-i \omega t} \phi \]
    So then we see that $\Phi(x, 2\pi / \omega) = \phi(x)$. So $\Phi$ is periodic.
\end{pf}

Now we go back to the original interpretation of the Liouville equation as composition with the flow of $X_H$. Looking at the last proposition, we can say
\[ \phi = \Phi(\cdot, 2 \pi / \omega) = \phi \circ \mathrm{Fl}_{X_H}^{2 \pi / \omega} \]
So if we impose some reasonable restrictions on the flow and $\phi$, we could get the following result:

\begin{prop}
    $\phi$ is an eigenvalue of $L_H$ with real eigenvaue $\omega$ $\iff$ $X_H$ has a periodic orbit of period $2 \pi / \omega$.
\end{prop}

\begin{pf}
    (Sketch of Proof) By the above proposition, we have that $\Phi(x,t)$, the solution to the Cauchy problem with initial data $\phi$, is periodic. We can write the solution additionally as the composition of $\phi$ with the flow. We get that
    \[ \phi \circ \mathrm{Fl}^{2 \pi / \omega} = \phi = \phi \circ Id \]
    If $\phi$ has a left inverse, we can cancel and conclude that the flow is the identity map; which is to say it has a periodic orbit.
\end{pf}

\indent It's clear that this proposition may not always hold. It depends on the existence of a real eigenvalue and the existence of a left inverse for $\phi$. This may be accomplished by restricting the domain of $\phi$ to make it injective, or some other trick, but it may not hold in general. Careful consideration of the function space is necessary, and topological concerns of the space may be necessary. \\
\indent Further work on this idea may be stated more generally and more vaguely as follow: What does the spectrum of the operator $L_H$ tell us about dynamics of $X_H$?\\
\indent There are other ways of playing around with the complex-valued Liouville equation as well. We may wonder that if we fix some compatible-almost complex structure $J$ on $M$, we can restrict the function space to 'almost holomorphic functions'$f$, defined by
\[ df \circ J = i df \Rightarrow df + i df \circ J =0 \]
Which can be viewed as a non-linear Cauchy Riemann equation. The similarity to pseudo-holomorphic curves is apparent. An almost holomorphic function that satisfies the complexified Liouville equation can then contain two solutions to the real Liouville equation. We may have a transformation, like the Hilbert transform, that transforms one solution into another linearly independent one. 


\subsection{Examples}
\indent \indent As an example, we examine the Liouville equation on $S^2$ with its usual symplectic structure. Since the manifold is compact, we can set our function space to $C^{\infty}(S^2)$ and every function here will be square-integrable. For a Hamitonian function, we will choose the height function
\[H: S^2 \to \mathbb{R}; H(x,y,z) = -z;\]
In symplectic coordinates $(\theta, \phi)$, given by the polar and azimuthal angle, we have that $H(\theta,\phi)=-\cos \phi$.

The Liouville Operator is then 
\[ L_H: C^{\infty}(S^2, \mathbb{C}) \to C^{\infty}(S^2, \mathbb{C}); \hspace{4pt} L_H f = i\sin \phi \frac{\partial f}{\partial \theta}\]
We then have the Liouville equation
\[ \frac{\partial F}{\partial t} = \sin \phi \frac{\partial F}{\partial \theta} \]
To find periodic orbits, we look for eigenvalues of $L_H$:
\[ i \sin \phi \frac{\partial f}{\partial \theta} = \omega f \]
We can separate variables in $\theta$ and $\phi$ and get
\[ f(\theta, \phi) = A(\omega,\phi) e^{-i \omega \theta \csc \phi} \]

We can see that the spectrum of $L_H$ is 'continuous,' i.e. every real number $\omega$ has an eigenvector. To construct a full solution, we integrate over all the $\omega$:
\[ F(\theta, \phi, t) = \int_{-\infty}^{\infty} A(\omega, \phi) e^{i \omega (t- \theta \csc \phi)} d\omega \]
We can recognize the form of the above expression as an integral transform, and see if we can find periodic orbits by considering the action of this integral operator on different function spaces or generalized function spaces. Consider $A(\omega, \phi) = \delta(\phi - \pi/2)$, where $\delta$ is the Dirac distribution on $S^2$. Then 
\[ F(\theta, \phi, t) = \delta(\phi-\pi/2) \delta(t-\theta \csc \phi) = \delta(\phi-\pi/2) \delta(t-\theta)\]
Which is delta distribution moving around the equator at frequency 1. \\
\indent We now see if we have a periodic orbit in the peirodic orbit in $X_H$. The vector field is 

\[ X_H = \sin \phi \frac{\partial}{\partial \theta} \]
So that Hamilton's equations of motion are:

\[ \dot{\theta} = \sin \phi \]
\[ \dot{\phi} = 0 \]
Which has solution $(\theta(t), \phi(t)) = ((\sin \phi)t \hspace{2pt}\mathrm{mod} \hspace{2pt} 2\pi, \phi_0)$. The flow then just rotates points about the $z$-axis with frequency $\sin \phi$. This is very similar to the result of the evolution of the Dirac Distribution under the Liouville equation. In fact, we can say that for a solution $(\theta(t), \phi(t))$ of the equations of motion, there is a solution to the Liouville equation:
\[ \Delta(\theta, \phi,t) = \delta(\theta(t), \phi(t)) \]
Which certainly follows the time evolution equation \ref{eqn:ham-flow}, and thus satisfies the Liouville equation.\\
\indent A more sophisticated application of the Lioville Equation might apply to counting geodesics. If we have a Riemannian manifold $(M,g)$, then we have an assocaited Hamiltonian system on the cotangent bundle with the canonical symplectic structure and the Hamiltonian function $H(p,p) = g(p^{\flat},p^{\flat})$. We can impose self-adjointness by demanding that $\frac{\partial ^2 g}{\partial q_i p_i} = 0$, then examine the spectrum of the operator $L_H$, which could act on, for example, $C^{1}(M) \cap L^2(M)$. We could even extend this to infinite dimensional manifolds, say for computing periodic solutions to the Euler equations of a perfect fluid, or some other infinite-dimensional Hamiltonian system. 

\subsection{The Liouville Equation as an Euler-Arnold Equation}

\indent Now we examine the form of the Liouville equation in light of the following fact: the natural Poisson bracket $ \{ , \} $ coming from the symplectic form, acting on $C^{\infty}(M)$, forms an abstract Lie algebra. Now examining the form of the Liouville equation

\[
	\frac{\partial f}{\partial t} = \{h,f\}
\]

we can recognize it as an \textit{Euler-Arnold equation}, which is to say, the Hamiltonian flow on a Lie group for some Hamiltonian function. We will proceed to analyze this viewpoint further, and follow the work of \cite{khesin2009} to fix an appropriate structure on this abstract infinite-dimensional Lie algebra. (Although the conventional idea of Euler-Arnold equations are as a flow on a Lie group, we will postpone what Lie group this Lie algebra corresponds to, and the details of the relationbetween them).\\
\indent Let $(N,\omega)$ be a closed symplectic manifold. Then there is a Poisson bracket $ \{ , \}_{\omega}$ arising from the symplectic form. Now consider the pair $( C^{\infty}(N), \{ , \}_{\omega})= \mathfrak{g}$ as an abstract Lie algebra. We define the \textit{isomorphic dual} (a subset of the full algebraic dual, which in this case is $ \mathcal{D}'(N)$) to this space as $ \mathfrak{g}^* = C^{\infty}(N)$, where the pairing is given $ \langle \xi, X \rangle = \int \xi(x) X(x) \Omega$, where $\Omega$ is the Liouville volume form (this is well defined for all vectors and covectors because $N$ is compact). We can also impose a 'frozen' \textit{Lie-Poisson structure} on the space $C^{\infty}( \mathfrak{g}^*)$ to make it a Lie algebra. This is given by 
\[
	\{ , \}_{h} : C^{\infty}( \mathfrak{g}) \times C^{\infty}( \mathfrak{g}) \to C^{\infty}( \mathfrak{g}^*); \hspace{4pt} (F,G) \mapsto \{ F,G \}_{h} = \langle {dF, dG}, h \rangle 
\]

Note that $dF \in \mathfrak{g}^{**} = \mathfrak{g}$ since we are only considering the isomorphic dual. Now we recognize the form of the Liouville equation as 
\[
	\dot{f_t}= \{ h,f_t \}_{\omega} = -\{ f_t, h \}_{\omega}  = -\mathrm{ad}^*_{f_t} h
\]

Now we can recognize that this is the Euler-Arnold equation for the frozen Lie-Poisson bracket for some function $ F: C^{\infty}( \mathfrak{g}) \to \mathbb{R}$ satisfying $dF_{f_t} = f_t$. But this has an obvious solution $F( \xi) = \frac{1}{2} \langle \xi, \xi \rangle $ (we identify, as an abuse of notation, $ \mathfrak{g}$ with $ \mathfrak{g}^*$).\\ 
\indent Now we can identify orbits of the Liouville equation with the Hamiltonian flow of a function under the particular frozen Lie-Poisson structure. To extend this further, if there is a symplectic form generating the frozen Lie Poisson structure, the quadratic Hamiltonian function generating the Liouville equation can be transferred to the geodesic flow of a left-invariant metric on the corresponding Lie group, the group of Hamiltonian flows on $N$.\\
\indent Now there are several comments to make on this interpretation. The first is about the dual space of the abstract Lie algebra $ \mathfrak{g}= ( C^{\infty}(N), \{ , \}_{\omega}$. The \textit{continuous dual} to this vector space, under the 'test function' topology (which makes $ \mathfrak{g}$ a Fre\`echet space), is the classical distributions $ \mathcal{D}'(N)$ on $N$. We would like a dual space that acts smoothly on the Lie algebra. Although we considered the 'isomorphic dual,' which is a direct copy of the Lie algebra with a well-defined bilinear mapping. However, we might be interested in the orbits of some less well-behaved initial conditions for the Liouville equation; for example, we could have $f_0 = \delta_x$, the evaluation map at the point $x$. This is physically important as it represents a point particle at the point $x$, and its evolution will describe the path of the particle under the Hamiltonian flow of $h$ on $N$. So for further applications, we may want to expand the type of dual space we consider.\\
\indent Now we would like to explain how the flows of $h$ on $N$ relate to the flows of $F$ on $ \mathfrak{g}^*$. We have seen that periodicities of the flow of $h$ create time-periodic solutions to the Liouville equation; we can then say that under appropriate conditions, we might have a one-to-one correspondence between periodic orbits of $h$ on $N$, and periodic orbits of $F$ on $ \mathfrak{g}^*$. This interplay could possibly reveal properties of each space and function not apparent from examining each alone.\\
\indent The last comment to make is that of the corresponding Lie algebra to $ \mathfrak{g}$. It is informally said that the group of Hamiltonian flows on $N$ form teh corresponding Lie group. However, these are infinite dimensional, and thus pathologies may occur. For example, the exponential map from the Lie algebra to the Lie group may not be well-behaved, as is possible with other infinte-dimensional Lie groups. Depending on the manifold $N$, we may find pathologies in the exponential map. This may obstruct further constructions on the group, its coadjoint orbits, etc.




\subsection{Action-Angle Coordinates}

\indent Hamiltonian system admitting action-angle coordinates are the nicest kind; the equations of motion become easily computable, and everything about the Hamiltonian system is instantly known upon transformation to action-angle coordinates. We would now like to relate the operator $L_H$ to action-angle coordinates and KAM tori.\\
\indent We will follow the presentation of action-angle coordinates as in Abraham and Marsden. Let $H$ be a Hamiltonian function on a symplectic manifold $(P,\omega)$. $H$ admits action angle coordinates on $U \subset P$, if there exists a symplectic map $\psi: U \to B^n \times \mathbb{T}^n$ satisfying
\[X_{\psi_*H} = - \sum_i \frac{ \partial H \circ \psi^{-1} }{\partial I^i}\frac{\partial}{\partial \phi_i} \]
Now we look at the transformed operator $L_H$ and examine its form in action-angle coordinates:
\[L_{H \circ \psi} = i \mathcal{L}_{X_H} = - i \sum \frac{ \partial (\psi_*H)}{\partial I^i} \frac{\partial}{\partial \phi_i}\]
Where $\frac{\partial}{\partial \phi_i}$ are now understood as partial differential operators rather than as tangent vectors. Note that the functions
\[\frac{\partial (\psi_*H)}{\partial I^i} = \nu_i(I^1, \ldots, I^n)\]
Are independent of the angular coordinates.



\bibliographystyle{unsrt}
\bibliography{stat-symp}







\end{document}
