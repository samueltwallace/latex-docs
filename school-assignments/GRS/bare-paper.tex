%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sample template for MIT Junior Lab Student Written Summaries
% Available from http://web.mit.edu/8.13/www/Samplepaper/sample-paper.tex
% Last Updated April 12, 2007
%  Edited by Robert DeSerio August 2010.
%
% Adapted from the American Physical Societies REVTeK-4 Pages
% at http://publish.aps.org
%
% ADVICE TO STUDENTS: Each time you write a paper, start with this
%    template and save under a new filename.  If convenient, don't
%    erase unneeded lines, just comment them out.  Often, they
%    will be useful containers for information.
%
% Using pdflatex, images must be either PNG, GIF, JPEG or PDF.
%     Turn eps to pdf using epstopdf.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREAMBLE

\documentclass[aps,twocolumn,secnumarabic,balancelastpage,amsmath,amssymb,nofootinbib,hyperref=pdftex]{revtex4}

% Documentclass Options
    % aps, prl, rmp stand for American Physical Society, Physical Review Letters, and Reviews of Modern Physics, respectively
    % twocolumn permits two columns, of course
    % nobalancelastpage doesn't attempt to equalize the lengths of the two columns on the last page
        % as might be desired in a journal where articles follow one another closely
    % amsmath and amssymb are necessary for the subequations environment among others
    % secnumarabic identifies sections by number to aid electronic review and commentary.
    % nofootinbib forces footnotes to occur on the page where they are first referenced
        % and not in the bibliography
    % REVTeX 4 is a set of macro packages designed to be used with LaTeX 2e.
        % REVTeX is well-suited for preparing manuscripts for submission to APS journals.


\usepackage{chapterbib}    % allows a bibliography for each chapter (each labguide has it's own)
\usepackage{color}         % produces boxes or entire pages with colored backgrounds
\usepackage{graphics}      % standard graphics specifications
\usepackage[pdftex]{graphicx}      % alternative graphics specifications
\usepackage[version=3]{mhchem}
\usepackage{longtable}     % helps with long table options
\usepackage{epsf}          % old package handles encapsulated post script issues
\usepackage{bm}            % special 'bold-math' package
\usepackage{verbatim}			% for comment environment
\usepackage[colorlinks=true]{hyperref}  % this package should be added after all others
                                        % use as follows: \url{http://web.mit.edu/8.13}

%\addtolength\topmargin{-.5\topmargin} %cuts the top margin in half.

%
% And now, begin the document...
% Students should not have to alter anything above this line
%

\begin{document}
\title{Measuring Gamma Radiation through Scintillation Spectroscopy}
\author{Samuel Wallace and Jacob Crowell}
\email{swallace6859@ufl.edu}
\date{\today}
\affiliation{University of Florida Department of Physics}


\begin{abstract}
	Gamma radiation is measured by a scintillation detector in order to measure activity rates, photopeak energies, and half-lives of various radioactive isotopes. Electrons scattered by Compton scattering with photons produce voltage spikes on the scintillator, which are then histogrammed by the voltage of the readings, producing a pulse height spectrum measuring the number of scattered electrons versus the number of electrons scattered. \ce{^{22}Na}, \ce{^{54}Mn}, \ce{^{60}Co}, \ce{^{137}Cs}, and \ce{^{152}Eu}  were all measured, for various quantities including activity rates, photopeak energies, and half-life. Excellent correlation in the data was achieved; the photopeak energies versus scaled voltage readings show an $R^2 = 0.9999$, and the half-life measurements give a $\chi^2_{\nu}= 1.33$. Activity rates also should vary with distance by a power law with dependence theoretically expected to be $ \frac{1}{r^2}$; measurement shows the exponent to have value $-1.98$ with sample standard deviation $0.131$. These measurements are deemed to show great correlation with theory. 
\end{abstract}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

\indent Radiation comes in many forms in physics, and have many different causes. Radioactive isotopes, atoms with uncommon quantities of nucleons, are known to give off several types of radiation as they decay into elements by breakdown of nucleons. Prominently, radioactive isotopes give off a certain spectrum of electromagnetic radiation, known as \textbf{Gamma radiation}. The purpose of the experiments described in the next section are to measure the properties of the spectra of radiation from various isotopes. \\
\indent Gamma radiation is a form of electromagnetic radiation. It can, but does not always, interact with matter. One particularly important type of interaction is known as \textbf{Compton scattering}. Compton scattering is the interaction of a photon and an electron involving an exchange of momentum and energy. It is a purely kinematic interaction; a photon and electron come in with known energy, momentum, and relative angle of impact, and leave with modified momentum, energy, and trajectory. The relationship between the photon's final energy, initial energy, scattering angle, and electron mass can be described by the following equation:
\[
	E_{\gamma}' = \frac{E_{\gamma}}{1+(E_{\gamma}/m_e c^2)(1- \cos \theta)}
\]
\indent This equation is derived in Appendix \ref{sec:eqn-der}.
\indent Electrons scattered by Compton scattering may undergo additional interactions within the the experimental apparatus (described in the next section). These notably include interaction via the photoelectric effect, where photons interacting with a sample of metal 'knock off' electrons from the sample. Another way that gamma radiation may interact is through pair production. Gamma radiation, when inside a strong electric field, may produce a electron-positron pair. The two scatter, and may collide with the scintillator, or the positron can collide with an electron and annihilate, producing another burst of gamma radiation.
\indent When measuring the radiation of radioactive isotopes, a quantity called the 'activity' is used to quantify how 'active' a source is. 1 curie (1 Ci) is a rate of $3.7 \times 10^{10}$ nucleon disintegrations per second. An observed count rate $R_E$ can be related to the activity of the source by the \textit{photopeak efficiency}, $p$, related by $R_E = \alpha p$. These quantities vary depending on the solid angle subtended by the detector, and so with a fixed area, the photopeak efficiencies and count rate should vary proportional to $ \frac{1}{r^2}$. 



\section{Experiment}

\indent The main detector of the experiment is a NaI scintillation detector. In brief, scattered electrons (through any of the mechanisms describe above) collide with a sample of NaI, creating photons. The produced photons go through an apparatus increasing the photons' intensities, which are then absorbed by an anode. The voltage across the anode is then read by an oscilloscope, and sent to a computer where a program produces a histogram-like plot of the voltage spikes across the anode. This plot forms the bulk of the collected data \cite{melissinos1966}.


\begin{figure}[h]

\centering
\includegraphics[width=0.4\textwidth]{./img/block-diag.png}
\caption{A block diagram for the scintillation detector.\cite{grslab} }
\label{fig:block-diag}

\end{figure}

\indent A block diagram for the whole measurement apparatus is shown in figure \ref{fig:block-diag}. It shows the general scheme for how the NaI detector feeds into the photomultiplier, is amplified, and then fed into the analyzer. Figure \ref{fig:schem} shows the physical setup of the detector and the sample trajectories of various gamma ray interaction. 



\begin{figure}[h]

\centering
\includegraphics[width=0.4\textwidth]{./img/schematic.png}
\caption{A schematic of the scintillator. \cite{grslab}}
\label{fig:schem}

\end{figure}


\indent Figure \ref{fig:schem} shows how radiation from the source impacts the NaI, creating photons which impact on the photocathode. The photomultiplier increases the strength of the radiation until it hits a dynode, creating a secondary emission of electrons, which then hit an anode, charging a capacitor, which quickly discharges, creating a voltage spike on the oscilloscope. A typical reading of the oscilloscope is shown in Figure \ref{fig:osc}.\\
\indent The oscilloscope feeds the computer its readings, and individual pulses are recorded by their voltage peak. The pulses are then histogrammed into 1024 bins ranging spanning the range of 0-10V. The data collection program is able to calculate various quantities related to the collected pulse data, including: total number of pulses within a certain bin range, the bin number of peaks in the histogram, the full-width half-max of a peak in the histogram.\\
\begin{figure}[h]
\centering
\includegraphics[{width=0.4\textwidth}]{img/oscilloscope.png}
\caption{A typical oscilloscope reading from the scintillator.}
\label{fig:osc}
\end{figure}
\indent Several types of measurements of several different isotopes were made, the first set of which are preliminary measurements to understand and familiarize the experimenters with the equipment. First, a spectrum of a \ce{^{137}Ba} as taken, with the source placed at a 'close' distance to the detector (10 cm), and a 'far' distance (30 cm). Next, a measurement of the detector's 'dead time' (time when the detector cannot accept new measurements while processing the previous measurement). The next set of measurements were to determine the contribution of background radiation separate from the source radiation; the radiation spectrum of the isotope alone can then be determined by removing the measured background contribution from the isotope spectra.\\
\indent The next set of measurement constitutes the bulk of the results, as they make very accurate measurements in order to determine important quantities of the radioactive samples. Measurements were then made to correlate bins with gamma radiation energies. An unknown element's spectra was measured. The measurements were made of several isotopes to determine their radioactive activity ($\alpha$) and photopeak efficiencies ($p$). The final set of measurements was to determine the half-life of \ce{^{137}Ba}, by taking a \ce{^{137}Ba} mild acid solution and taking a quick succession of spectra. 


\section{Results}
\indent The spectrum of \ce{^{137}Cs} was measured at both close distance and far distance. The spectra were qualitatively seen to have a broader peak at a lower voltage when the sample was moved to the far position (data is not included for these results as the background radiation interferes with count rates and photopeak data). The same sample was then used to measure the dead time per pulse. After a 60s real time spectra collection, a live time of 48.66s was achieved, and with a total pulse count of $\approx 1.44 \times 10^6$, the detector has a dead time of 7.02 $\mu s$ per pulse. A percentage dead time of 9.25 \% was achieved when the \ce{^{137}Cs} source was moved to 10 cm from the detector. \\
\indent The next set of results are related to background spectra correction. The \ce{^{137}Cs} sample spectrum was taken, with the background radiation stripped from the spectrum, and is shown in Figure \ref{fig:bground-spec}.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{img/137Cs-step17.pdf}
\caption{The background corrected \ce{^{137}Cs} spectrum. }
\label{fig:bground-spec}
\end{figure}

\indent Now the correlation between photopeak energies and voltage bin was sought. Samples of \ce{^{137}Cs},\ce{^{137}Ba}, \ce{^{22}Na}, \ce{^{60} Co}, and \ce{^{152} Eu} had their spectra measured at varying distances, all at distances gave low dead time (i.e. $<10\%$) and stable photopeak energies. The photopeak voltage bins are shown in table \ref{tab:e-cal}.

\begin{table}[h]
\centering
\begin{tabular}{ccc}

	Sample & Photopeak Bin & FWHM \\
	\hline \hline 
	\ce{^{137}Cs} & 219.19 & 19.35 \\
	\ce{^{22}Na} & 164.2 & 17.73 \\
	\ce{^{60} Co} & 380.59 & 13.41 \\
	\ce{^{152} Eu} & 43.11 & 7.17 \\
	\hline \hline


\end{tabular}
\caption{Photopeak Bins of Largest Photopeaks and their FWHM}
\label{tab:e-cal}
\end{table}

\indent The energies of the prominent photopeaks were referenced from \cite{duggan1988} and linear regression was used to fit the bin number with the photopeak energies. The linear fit had a correlation coefficient $r = 0.9999$, and the proportionality factor was estimated at $3.069 \pm 0.016$ keV/V. The sample of the unknown element was measured and its spectrum is shown in Figure \ref{fig:mystery}. The main photopeak has an energy (derived from the correlation) of $1471.19 \pm 8.5$ keV. It is believed to be \ce{^{40}K}, as its main photopeak energy is 1460.8 keV, and this is the element with the closest photopeak energy that has a half-life consistent with the age of the sample.\\

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{img/unknown-sample-spectrum.pdf}
\caption{The radiation spectrum of the unknown element.}
\label{fig:mystery}
\end{figure}

The activity measurements and photopeak efficiences of the various samples are shown in tables \ref{tab:2014-Na} and \ref{tab:2008-Na}. These values were used to determine the exponent of a power law variation of photopeak efficiencies with distance. The exponent was calculated as $-1.98 \pm 0.131$. 

\begin{table}[h]
\centering
\begin{tabular}{cccc}

	Distance (cm) & $\alpha$ ($s^{-1})$ & $p_1$ ($s^{-1}$) & $p_2$ ($s^{-1}$) \\
\hline \hline
15.0 & 9.60 $\times 10^4$ & 5.27 $\times 10^3$ & 1.58 $\times 10^3$ \\
25.0 & 8.41 $\times 10^4$ & 1.95 $\times 10^3$ & 548 \\
35.0 & 6.93 $\times 10^4$ & 938 & 266 \\
\hline \hline

\end{tabular}
\caption{Activity Measurement and Photopeak Efficiences of a 2014 Sample of \ce{^{22}Na}}
\label{tab:2014-Na}
\end{table}

\begin{table}[h]
\centering
\begin{tabular}{cccc}

	Distance (cm) & $\alpha$ & $p_1$ & $p_2$ \\
	\hline \hline
	15.0 & 1.92 $\times 10^4$ & 1.22 $\times 10^3$ & 306 \\
	25.0 & 1.46 $\times 10^4$ & 441 & 143 \\
	35.0 & 1.52 $\times 10^4$ & 219 & 65.3 \\
	\hline \hline

\end{tabular}
\caption{Activity Measurements and Photopeak Efficiences of a 2008 Sample of \ce{^{22}Na}}
\label{tab:2008-Na}
\end{table}

Next, the half-life of \ce{^{137}Ba} was measured. By rapidly taking spectra measurements in succession, data was collected on activity rates versus time. By taking 150 spectra over the course of 27 minutes, a half-life of $2.53 \pm 1.93 \times 10^{-5}$ was measured with $\chi^2_{\nu} = 1.33$. A graph of the data and the fitted exponential curve is shown in figure \ref{fig:half-life}. The half-life was measured as $2.53 \pm 0.131$ min.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.45\textwidth]{./img/half-life.png}
	\caption{Fitted Exponential Decay of \ce{^{137}Ba}}%
	\label{fig:half-life}
\end{figure}

\section{Conclusions}
\indent The calibration of the apparatus showed great success. Reduction of dead time was to a desired amount, changes in spectra quality were seen at different lengths and amplifier voltages, and background spectrum removal qualitatively revealed clear peaks in the radiation coming from the sample. In addition, the correlation between peak bins and photopeak energies was excellent, and gave a strong prediction for the unknown sample.\\
\indent In addition, the results matched theory well. The dependence on activity rate versus distance showed agreement with the theoretical expectation of a inverse square law dependence. The Barium sample half-life also showed agreement with the accepted value of 2.55 minutes.

\bibliography{sample-paper}

\appendix

\section{Derivation of the Energy Relations}
Let $E_{\gamma}$, $E_e$ be the energies of the photon and electron before the collision, and let $E_{\gamma'}$, $E_{e'}$ be the energies of the photon and electron after the collision. Then conservation of energy dictates that
    \[ E_{\gamma} + E_{e} = E_{\gamma'} + E_{e'} \]
\label{sec:eqn-der}

    And if $p_{\gamma}, p_{\gamma'}, p_{e'}$ are the respective momenta of the photon before collision, photon after collision, and the electron after collision (the inital momentum of the electron is zero), then conservation of momentum dictates that:
    \[ \vec{p}_{\gamma} = \vec{p}_{\gamma'} + \vec{p}_{e'} \]
    Using the Einstein relation and Planck's formula:
    \[ E_{\gamma} = h f; \hspace{4pt} E_{\gamma'}=h f' \]
    \[ E_e = m_e c^2; \hspace{4pt} E_{e'} = \sqrt{(p_{e'}^2c)^2+(m_e c^2)^2} \]
    Now conservation of energy reads
    \[ hf + m_e c^2 = hf' + \sqrt{(p_{e'}^2)^2+(m_ec^2)^2} \]
    Which can be solved for the magnitude of the momentum:
    \[ p_{e'}^2 c^2 = (h (f-f')+m_ec^2)^2 - m_e^2 c^4 \]
    Now we use conservation of momentum. Taking the magnitude of the electron's final momentum, $p_{e'}^2 = \vec{p}_{e'}\cdot \vec{p}_{e'}$, and using the above formula for momentum conservation, we get:
    \[ p_{e'}^2 = p_{\gamma}^2 + p_{\gamma'}^2 - 2 p_{\gamma}p_{\gamma'} \cos \theta \]
    Then we multiply both sides by $c^2$ and set the equation equal to the result from conservation of energy:
    \[ (h (f-f')+m_ec^2)^2 - m_e^2 c^4 = p_{e'}^2 c^2 \] 
    \[= p_{\gamma}^2 c^2 + p_{\gamma'}^2 c^2 - 2 p_{\gamma}p_{\gamma'} c^2 \cos \theta \]
    \[ = (hf)^2 + (hf')^2 - 2 h^2 f f' \cos \theta \]
    Which, after equating far left and far right sides and rearranging, results in 
    \[ 2hfm_e c^2 - 2hf'm_e c^2 = 2h^2 f f' (1- \cos \theta)\]
    Which we can solve for $f$ and use the Planck formulae to turn the frequencies back into energies:
    \[ E_{\gamma'} = hf' = \frac{hf m_e^2 c^2}{m_e c^2 + hf (1-\cos \theta)} \]
    \[= \frac{m_e c^2 E_{\gamma}}{m_e c^2 + E_{\gamma}(1-\cos \theta)}\]
    And dividing the final formula through by $m_e c^2$ gives the desired formula. 


\end{document}
