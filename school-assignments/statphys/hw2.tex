\documentclass[notitlepage, twocolumn]{article}
\usepackage[utf8]{inputenc}

\usepackage{gensymb}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}

\title{PHY 4523 Homework Set 2}
\author{Samuel Wallace}
\date{\today}

\begin{document}

\maketitle

\begin{enumerate}
    \item First we will derive the entropy change of the universe of a process where a certain amount of water at temperature $T_i$ coming into contact with a heat bath at temperature $T_{bath}$, then use this algebraic result to simplify the remaining problems.\\
    \medskip
    Suppose a certain amount of water with initial temperature $T_i$ comes into contact with a heat bath at temperature $T_{bath}$. There will be heat exchange, and finally the two will reach thermal equilibrium. The water has constant heat capacity $C$. Calculating from the equation for entropy:
    \[ \Delta S_{water} = \int_{T_1}^{T_2} \frac{\delta Q}{T} = \int_{T_i}^{T_{bath}} \frac{C dT}{T} \]
    \[= C \mathrm{ln}(T_{bath}/T_i)\]
    Where $T_2 = T_{bath}$ because the heat bath has negligible change in temperature and the two systems come into thermal equilibrium. And for the heat bath:
    \[ \Delta S_{bath} = \int_{T_1}^{T_2} \frac{\delta Q}{T} = \int_{T_{bath}}^{T_{bath}} \frac{\delta Q}{T_{bath}} = 0\]
    Because the initial and final temperatures are the same. Thus the total change in entropy of the universe: 
    \[\Delta S_{Univ} (T_{bath}, T_i) = C \mathrm{ln}(T_{bath}/T_i)\]\\
    \medskip Now we will begin with the numerical calculations. Note that entropy of a concatenation of processes is the sum of the entropies of the individual processes; we will use this repeatedly in the calculations.
    
    \begin{enumerate}
        \item With $T_i = 0 \degree C = 273.15 K$ and $T_{bath} = 100 \degree C = 373.15 K$, we have 
        \[\Delta S_{Univ} = C \mathrm{ln}(373.15/273.15) = 0.312 C\]
        \item With two processes, we calculate a first entropy change of the universe $\Delta S_{Univ}^{(1)}$ and a second $\Delta S_{Univ}^{(2)}$:
        \[\Delta S_{Univ}^{(1)} = C \mathrm{ln}(323.15/273.15) = 0.168 C\]
        \[
        \Delta S_{Univ}^{(2)} = C \mathrm{ln}(373.15/323.15) = 0.144 C
        \]
        Which gives a total of $0.312 C$. 
        \item Calculating:
        \[ \Delta S_{Univ}^{(1)} = 0.0876 C \]
        \[ \Delta S_{Univ}^{(2)} = 0.0805 C \]
        \[\Delta S_{Univ}^{(3)} = 0.0745 C\]
        \[\Delta S_{Univ}^{(4)} = 0.0693 C\]
        Totaling, we get the same $0.312 C$.
        \item Consider a subdivision of the range $273.15 < T < 373.15$ by temperatures $T_n$, with $T_{n+1} > T_n$ that eventually converge to the upper temperature (For example, take $T_i = (1 - \frac{1}{i})(100K) + 273.15K$). Then we examine the sequence of processes where the water begins at temperatures $T_i$ and comes into contact with a heat bath of temperature $T_{i+1}$. The associated entropy change of this process is:
        \[
        \Delta S_{Univ}^{(i)} = C \mathrm{ln}(T_{i+1}/T_i)
        \]
        And we now need to evaluate the sum of all the entropies:
        \[\sum_{i=1}^{\infty} \Delta S_{Univ}^{(i)} = \lim_{n \to \infty} \sum_{i=1}^{n} \Delta S_{Univ}^{(i)}\]
        We evaluate partial sums by induction. I claim that
        \[
        \sum_{i=1}^{n} \Delta S_{Univ}^{(i)} = C \mathrm{ln}(T_{n+1}/T_1)
        \]
        This is clearly true for $n=1$, as this would reduce to the original formula for the $n^{th}$ entropy change. Now we examine the general case. Assume the above holds; we show it holds for $n+1$. 
        \[
        \sum_{i=1}^{n+1} \Delta S_{Univ}^{(i)} = \Delta S_{Univ}^{(n+1)} + \sum_{i=1}^{n} \Delta S_{Univ}^{(i)} \]
        \[= C \mathrm{ln}(T_{n+2}/T_{n+1}) + C \mathrm{ln}(T_{n+1}/T_1) \] 
        \[= C \mathrm{ln}\left( \frac{T_{n+2}}{T_{n+1}} \cdot \frac{T_{n+1}}{T_1} \right) = C \mathrm{ln}(T_{n+2}/T_1)
        \]
        
        And by induction the formula holds. So then 
        \[
        \sum_{i=1}^{\infty} \Delta S_{Univ}^{(i)} = \lim_{n \to \infty} C \mathrm{ln}(T_{n+1}/T_1) \]
        \[= C \mathrm{ln}(373.15/273.15) = 0.312 C
        \]
        Because the $T_{n} \to 373.15K$. Thus for any infinite subdivision of the original process, the entropy change is the same.
        
    \end{enumerate}
    
    \item Thinking of the "gaps" between adjacent spins as a "slot" for a potential domain wall, there are $N$ ways to place the first, $N-1$ ways to choose the second $\ldots$ $N-n+1$ ways to place the $n$-th domain wall. Thus in total there are $N \cdot (N-1) \cdot \ldots \cdot (N-n+1) = \frac{N!}{(N-n)!}$ microstates. Thus the entropy is $S(n) = k_B \mathrm{ln} \left( \frac{N!}{(N-n)!} \right)$. Assuming $n, N >> 1$, we can simplify with Stirling's Inequality:
    \[
    \mathrm{ln}(N!) - \mathrm{ln}((N-n)!)\]
    \[= N \mathrm{log}(N) - N - (N-n)\mathrm{ln}(N-n) + N-n\]
    \[
    =N \mathrm{ln}\left( \frac{N}{N-n} \right) + n \mathrm{ln}(N-n) - n
    \]
    \[
    =N \mathrm{ln}\left( \frac{N \epsilon}{N\epsilon-U} \right) + \frac{1}{\epsilon} (U\mathrm{ln}(N-U/\epsilon) - U)
    \]
    So then
    \[
    S(U) = k_B \left[ N \mathrm{ln}\left( \frac{N \epsilon}{N\epsilon-U} \right) + \frac{1}{\epsilon} (U\mathrm{ln}(N-U/\epsilon) - U) \right]
    \]
    Then using $\frac{1}{T} = \frac{\partial S}{\partial U}$, we have:
    \[
    \frac{1}{T} = S'(U) \]
    \[= \frac{k_B N}{N \epsilon - U} - \frac{k_B U/ \epsilon}{N \epsilon - U} + (k_B/\epsilon)( \mathrm{ln}(N- U/\epsilon) -1)
    \]
    \[
    =(k_B/\epsilon)\mathrm{ln}(N- U/\epsilon)
    \]
    Solving for U, we get the desired expression:
    \[
    U = \frac{N \epsilon}{1+\exp{\epsilon/k_B T}}
    \]
    A graph of the $U$ vs $\mathrm{ln}(k_B T /\epsilon)$ is shown below.
    \begin{figure}[h]
        \centering
        \includegraphics[width=0.5\textwidth]{U(T).png}
        \label{fig:my_label}
    \end{figure}
    There is the logistic function; it shows a sharp increase in the middle of two areas of negligible change.\\
    The heat capacity is $T \frac{\partial S}{\partial T} = T \frac{\partial S}{\partial U}\frac{\partial U}{\partial T}$. Calculating,
    \[
    T S'(U) U'(T) = T \frac{1}{T} U'(T) \]
    \[= -\frac{\epsilon}{k_B T^2} \cdot \frac{-N \epsilon \exp{\epsilon/k_B T}}{(1+ \exp{\epsilon/k_B T})^2 }
    \]
    Which looks like 
    \begin{figure}[h]
        \centering
        \includegraphics[width=0.5\textwidth]{U'(T).png}
        \label{fig:my_label}
    \end{figure}
    
    \item Calculating,
    \[\frac{1}{T} = \left. \frac{\partial S}{\partial U} \right)_V\]
    \[= \sigma V^{1/4} U^{-1/4} \Rightarrow T = \frac{ V^{1/4}}{\sigma U^{1/4}} = \frac{4 U^{1/2}}{3 S}\]
    \[
    \frac{4U^{1/2}}{3T} = S = \frac{4 \sigma V^{1/4} U^{3/4}}{3}
    \]
    \[
    \Rightarrow U =  \sigma^4 V^{1/3} T^4
    \]
    \[
    P = \frac{\partial U}{\partial V} = \sigma^4 \frac{1}{3} V^{-2/3} T^4 = U/3V \]
    \[
    \Rightarrow PV = U/3
    \]
    
\end{enumerate}
\end{document}
