\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Fluid Mechanics Background and History}{2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Experimental Understanding of Turbulence}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Theoretical Turbulence and Statistical Mechanics}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Physical Justification for Statistical Methods}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Big Ideas}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Results of Statistical Methods}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Conclusion}{10}
