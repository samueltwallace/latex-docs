\documentclass{article}

\usepackage[style=alphabetic, sorting=nty]{biblatex} 
\usepackage{graphicx} 
\usepackage{wrapfig} 

\linespread{1.5}

\title{Statistical Mechanics and Turbulence}
\author{Samuel T. Wallace}
\date{\today}

\addbibresource{main.bib}

\begin{document}
\maketitle
\tableofcontents

\section{Introduction}
\indent \indent Turbulence is disordered fluid flow patterns that change chaotically. Turbulence is an easy to observe phenomena that has proven itself difficult to understand. It is one of the last few problems in classical mechanics. Turbulence is an important phenomena, with many engineering applications, mathematical implications, and physical importance. It is present in the kitchen sink, to cloud movement, to interstellar gases. It still remains beyond a complete description.\\
\indent Understanding turbulence would be a huge gain in many areas of science; its ubiquity should make this clear. Understanding turbulence, for engineering, would revolutionize design of air foils on aircraft wings, mixing of chemicals in vats, and pressure drops across pipe junctures. In mathematics, new mathematical tools must be developed to understand the nonlinear partial differential equations that govern turbulence, which will shed light on a wide variety of similar equations. In physics, turbulence is important for understanding plasma dynamics, which has implications on star formation and evolution. Fluid mechanics, generally, has wide applications, in biology for understanding flow through capillary blood vessels and blood oxygenation. Even programmers are interested in turbulence for creating realistic looking simulations of bodies of water, dust clouds, and fire. \\
\indent This paper is structured as follows: first is relevant fluid mechanics background and history. Next physical observations and experiments studying turubulence, then finally theoretical descriptions of turbulence and the methods of statistical mechanics applied to turbulence.

\section{Fluid Mechanics Background and History}
\indent Fluid flow has a familiar mathematical description: a time-dependent vector field. This is a vector field depending on space and time that tells the velocity of particles moving through that point in space. Our main object of interest will be this vector field $ V(x,y,z,t) $, where $ \left( x,y,z \right) $ is a point in $ \Omega $.\\
\indent A careful application of Newton's second law to the Lagrangian picture yields a formula for the time evolution of the fluid velocity vector field, called the \textbf{Navier-Stokes Equations} (NSE):
\[
	\frac{ \partial (\rho V)}{\partial t } + \left( V \cdot \nabla \right) ( \rho V) = - \nabla p + \mu \nabla^{2} V
\]
\[
	\nabla \cdot ( \rho V) = 0
\]
Where $ \rho(x,y,z,t) $ is the mass density of the fluid, $ p(x,y,z,t) $ is the pressure exerted by the fluid and other forces, and $ \mu $ is the viscosity of the fluid. The form of the equations is quite familiar; the first one expresses convervation of momentum per unit volume; the second expresses conservation of mass. Solving these equations exactly would solve every single fluid mechanics problem, and the solutions could be studied for important features. However, there are very few known solutions to the NSE, even when massive mathematical and physical assumptions are made. It is currently an open problem (with an offered monetary prize) for proving that smooth initial conditions for the NSE will not become a singularity at some finite time in the future. The solution to the NSE would be revolutionary for the study of turbulence. \\
\indent Fluid mechanics often involves dimensionless parameters to help characterize fluid flow. These often come from analysis of the NSE and finding a parameter that will eliminate a term, simplifying the equations, sometimes making them soluble. The most commonly used parameter for identifying turbulence is the \textbf{Reynold's Number}:
\[
\mathrm{Re} = \frac{ \overline{\rho} \overline{V} L}{ \mu} 
\]
Where $ \overline{ \rho} $ is a density closely associated with the fluid (for example, average density over space), $ \overline{V} $ is a speed closely associated with the fluid body (for example, average velocity), $ L $ is a length closely associated with the fluid geometry (for example, average diameter of the fluid volume), and $ \mu $ is the viscosity of the fluid. \\
\indent It is important to mention that there is no generally accepted rigorous defintion of turbuence \cite{arstechnica}. It is more of a field of study rather than a specific pattern \cite{falkovich2006}. A common feature of turbulent flow is high Reynold's number, though there is no common idea for what critical value the Reynold's number must reach for the fluid flow to be "turbulent." The idea of turbulence, however, has reached beyond simply fluid flow, and is now a term applied to several areas of nonlinear physics, including non-linear optics and the study of Bose-Einstein condensates and plasmas driven far from equilibrium \cite{cardy2008}.

\section{Experimental Understanding of Turbulence}

\indent Turbulence is observed often when steady-state fluid flow (laminar flow) is driven beyond steady state and turbulent flow develops. We will study several common type instability of in fluid flow as a model for experimental investigations of fluid flow. \\
\begin{wrapfigure}{l}{0.5\textwidth}
	\centering
	\includegraphics[width=0.45\textwidth]{KHI.png}
	\caption{Kelvin-Helmholtz Instability visualized with smoke in a wind tunnel. Time runs forward looking from top to bottom. From \cite{falkovich2006}.}
	\label{KHI}
\end{wrapfigure}

\indent \textbf{Kelvin-Helmholtz instability} is a fluid flow instability that presents itself at an interface of two parallel streams of fluid flow. The two streams develop `curls,' and then begin to mix, until at higher speeds, the flows mix almost completely. This is a classical example of turbulence. \\


\indent Kelvin-Helmholtz instability is a type of turbulence that is commmon. It can show up in the wakes of vehicles, the internal compartments of motors (which ensures good mixing of fuel and air), and even in river and waterway flow patterns \cite{terradekelvin}. Kelvin-Helmholtz instability also shows up in interstellar clouds, clumping of supernova debris, and as an instability of high-density plasmas \cite{vujinovic2015}. \\

\indent Experimental interest in turbulent flow patterns is the quality of the instability shape. Kelvin-Helmholtz instability is known for initially keeping a two-dimensional vorticial shape before becoming more turbulent. The measure for turbulence in this type of instability is the \textit{Richardson number}, another dimensionless quantity related to the rate at which buoyant forces oscillate fluids displaced from equilibrium, and the change in speed across the boundary \cite{vujinovic2015}. \\
\indent Kelvin-Helmholtz instability shows up in more complex forms, as well. It can show up as a \textit{Von Karman alley}, which is a trail of Kelvin-Helmholtz eddies in the wake of an obstacle in flowing fluid. There is a discontinuity in the flow velocity in the wake of the object, creating a `slipstream' behind it. There is a net rotation of fluid in place (called vorticity) that creates double layers of eddies. It can be seen in the ocean, as in the wake of boats, or notably as the culprit of the 1940 collapse of the Tacoma Narrows bridge, where Stroual instabilites causing Von Karman alleys detached from the wake of the obstacle and continued moving, causing the eventual collapse of the bridge \cite{terradestroual}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Stroual.png}
	\caption{Stroual instability behind the wreck of a boat. \cite{terradestroual}.}
	\label{fig:Stroual}
\end{figure}



\section{Theoretical Turbulence and Statistical Mechanics}

\indent The first scientist to approach turbulence from a statistical point of view was the Russian mathematician Andrey Kolmogorov in his paper \cite{kolmogorov} in the early 1940s. The next big pioneer was Belgian-French mathematical physicist David Ruelle, who has spent nearly the entirety of the latter half of his career working on an approach to turbulence based on non-equilibrium statistical mechanics. I will mainly explain from one of his most recent and well-developed papers \cite{ruelle}. \\
\indent We will first explain a physical justification for using methods from statistical mechanics to explore turbulence, then highlight the big ideas of Kolmogorov's and Ruelle's work, then state the conclusions these methods brought them. \\

\subsection{Physical Justification for Statistical Methods}

\indent It may not be initially clear why methods from statistical mechanics may provide insight into fluid dynamics. This section aims to clear up why we might expect results. \\
\indent Fluid mechanics shares many similarities physically with classical problems of statistical mechanics. Classical statistical mechanics is, very broadly speaking, the bulk properties of many particles that interact. Fluid mechanics fits this definition very well, so we could expect ideas from statistical mechanics to be applicable to fluid mechanics. There is a difference between the usual applications of statistical mechanics and fluid mechanics. Statistical mechanics is usually applied to systems where motions of individual particles cannot be solved for, while fluid mechanics, if solved, would provide information on the movement of individual particles. So fluid mechanics and statistical mechancis have not the same goals as fields of study. \\
\indent However, turbulence is a good phenomenon to apply statistical mechanics to. For one, it is unrealistic to expect a complete solution to the Navier-Stokes Equations for turbulent flow. It is also useful to have bulk properties of turubulent flow to measure quantities such as energy dissipation, pressure drops, entropy production. And probably most importantly, turbulence is a macroscopic phenomenon that changes in response to external surroundings; this likens the transition of laminar flow to turbulent as a \textit{change of phase} for a system of fluids. To expand on this a little more, fluid flow, when looked at from a macroscopic perspetive, can be viewed as a system with functions of state related to the dimensionless quantities that have been introduced (Reynold's number, Richardson number), and these can be related to macroscopic observations such as transition to varying degree of turbulence, interpreted as a change of phase of the fluid system. Viewed from this perspective, statistical mechanics seems like a very useful tool for the study of turbulence.\\



\subsection{Big Ideas}

\indent Kolmogorov's approach, in the large, is to treat the turbulent fluid velocity field $ V $ as  random variable, in the sense that it is drawn from a  random distribution of velocities according to some distribution. Then he sets out several hypotheses for the relation between these probability distributions and fluid properties (viscosity, Reynold's number, etc.). His hypotheses are as follows:

\begin{enumerate}
	\item For turbulent flow with no net motion, the distribution of velocities is determined completely by the viscosity of the fluid and the gradient of average velocity in each direction. 
	\item For large displacement within the fluid relative to velocity fluctuations that lead to heat loss through internal friction, the distribution does not depend on viscosity.
\end{enumerate}

These are results that rely on mathematical assumptions rather than physical simplifications, but are a good first step for developing a theory of turbulence. \\
\indent Ruelle's approach is much more sophisticated. His aim is to determine the velocity distributions for a turbulent fluid. His main idea is that turbulent fluctuations have the same governing equations as a mechanical system with thermal fluctuations for some mechanical system. A fluid system is represented as a collection of finite-volume subsystems. The size of the subsystems is varied in order to eliminate terms in the NSE for turbulent interactions. In fact, in earlier work, for inviscid flow, the simplified NSE can be shown to have a Hamiltonian function, implying conservation of energy. Then, by dividing up the cells by volume, we can think of the 'nicely behaved' cells as having Hamiltonian dynamics, which the others have external forcing from other cells. Though equations of motion cannot be solved, this provides a basic framework for understanding the propagation of turbulence through the different cells. \\
\indent Viewed this way, we can think of the kinetic energy of turbulent flow as analogous to 'heat' through different subsystems; we can calculate the classical thermodynamics of such a system, and translate back to fluid mechanics. However, calculating the thermal fluctuations of a non-equilibrium system is a difficult task, so an assumption is made to ease calculation: that cells are in approximate equilibrium. This allows the use of the Boltzmann distribution to look for average energy. We thus have a distribution of the form:
\[
	~ \mathrm{exp} \left( - \frac{ \vert V \vert^{3}}{V_{n} \kappa^{-1}}  \right)
\]
Where $ V $ is the average velocity in a cell, $ V_{n} $ is the volume of a cell, and $ \kappa $ is a dimensionless constant related to the number of particles and the size of the cells. This allows a treament of the mechanical system as a canonical ensemble expected to be reasonable for large volumes and large numbers of cubes. \\
\indent These are the basic ideas for each approach to turbulence. In the next section we will show what results these approaches bring.


\subsection{Results of Statistical Methods}

\indent Here we will go over what cumulative results the previously introduced approaches bring. We will mainly cover Ruelle's results, as these develop both from his work and Kolmogorov's work; and as such, represent nearly the totality of work thus far of statistical mechanics and turbulence.\\
\indent A big result of Ruelle's is to show an exponent for energy dissipation due to turbulence effects. He obtains the result that
\[
\langle \epsilon^{p}_{l_{n}} \rangle \approx l_{n}^{ \tau_{p}}
\]
with 
\[
\tau_{p} = - \frac{ \mathrm{ln} p!}{ \mathrm{ln} \kappa} 
\]
Where $ \epsilon_{l_{n}} $ is the energy dissipation in a cube of length $ l_{n} $, so that the above exponent $ p $ gives \textit{moments} of the distribution. For example, plugging in $ p = 2$ gives the variance of the energy dissipation. Also obtained is an explicit formula for the distribution of velocities in a collection of cells $ S_{n} $ (the composition of the collection of cells is more complicated than needs explanation), given by
\[
	\mathrm{ln} \varpi (S_{n}) \approx n \kappa^{4} \mathrm{Re}^{-3/n}
\]
which says that as we increase the number of cells in a collection, the probability distribution decreases exponentially. Notice also that the probability distribution increases with $ \mathrm{Re} $, the Reynold's number. \\
\indent The last major result of Ruelle's work is to provide an estimate for the distribution describing radial velocity gradient, $ \frac{ \partial V}{\partial r }  $, which would tell us of the large scale behavior of turbulence and give easier comparison to experiment. He work through several variations of the distribution, starting with one that looks like 
\[
	F_{ \Delta}(V; V_{0}) = \left( \prod_{k=1}^{N} \int_{0}^{\infty} \frac{ \kappa dV_{k}}{V_{k-1}} e^{- \kappa V_{k}/V_{k-1}} \right) \Phi_{ \Delta} (V; V_{1}, \ldots V_{N})
\]
Where $ \Phi_{ \Delta} $ is the distribution of the $ x $-component of velocity in the $ N $ volume cells $ V_{1},\ldots, V_{N} $ (notice the appearance of the Boltzmann distribution here as part of the calculation of the velocity distribution). A second variant of velocity distribution is defined by a change of variables $ y = ( \kappa^{n}/V_{0})^{1/3} \vert V \vert $, giving probability distribution $ G_{n}(y) $. Then the most physically representative probability distribution is derived:
\[
	p_{n}(z) = \frac{1}{2} \left( \frac{1}{3} \Gamma \left( \frac{5}{3}  \right)^{n} \right)^{1/2} \cdot G_{n} \left( \left( \frac{1}{3} \Gamma \left( \frac{5}{3}  \right)^{n} \right)^{1/2} \vert z \vert \right)
\]
Where $ \Gamma $ is the mathematician's Gamma Function; $ \Gamma(n) = (n+1)! $. Ruelle notes that the graphs of these functions have certain inflection points that can be seen in experimental data (although the points do not quite match up - a discussion of more subtle effects follow). However, there is substantial agreement of this theory and experimental data.


\section{Conclusion}

Statistical mechanics proves itself to be another valuable tool for the hydrodynamicist. Though Ruelle's work is recent and the calculations are very involved, they are proof of concept that statistical methods lead to insights into turbulent flow. These results have yet to be turned into numerical or computational tools, but the results might give light in application. \\
\indent Because of turbulence's ubiquity, we should expect these results to shed light on the many applications where turbulence is present. We could see calculation of eddy lifetimes in the wake of an airfoil, using the energy dissipation law derived. There may also be applications in the supernova applications mentioned by use of the radial gradient distribution law. \\
\indent Turbulent flow continues to be a mystery at its core. Even though statistical methods is not a complete answer, it provides valuable and applicable information for one of the most common phenomenon in the universe.

\printbibliography

\end{document}

