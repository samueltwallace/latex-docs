%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sample template for MIT Junior Lab Student Written Summaries
% Available from http://web.mit.edu/8.13/www/Samplepaper/sample-paper.tex
% Last Updated April 12, 2007
%  Edited by Robert DeSerio August 2010.
%
% Adapted from the American Physical Societies REVTeK-4 Pages
% at http://publish.aps.org
%
% ADVICE TO STUDENTS: Each time you write a paper, start with this
%    template and save under a new filename.  If convenient, don't
%    erase unneeded lines, just comment them out.  Often, they
%    will be useful containers for information.
%
% Using pdflatex, images must be either PNG, GIF, JPEG or PDF.
%     Turn eps to pdf using epstopdf.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREAMBLE

\documentclass[aps,twocolumn,secnumarabic,balancelastpage,amsmath,amssymb,nofootinbib,hyperref=pdftex]{revtex4}

% Documentclass Options
    % aps, prl, rmp stand for American Physical Society, Physical Review Letters, and Reviews of Modern Physics, respectively
    % twocolumn permits two columns, of course
    % nobalancelastpage doesn't attempt to equalize the lengths of the two columns on the last page
        % as might be desired in a journal where articles follow one another closely
    % amsmath and amssymb are necessary for the subequations environment among others
    % secnumarabic identifies sections by number to aid electronic review and commentary.
    % nofootinbib forces footnotes to occur on the page where they are first referenced
        % and not in the bibliography
    % REVTeX 4 is a set of macro packages designed to be used with LaTeX 2e.
        % REVTeX is well-suited for preparing manuscripts for submission to APS journals.


\usepackage{chapterbib}    % allows a bibliography for each chapter (each labguide has it's own)
\usepackage{color}         % produces boxes or entire pages with colored backgrounds
\usepackage{graphics}      % standard graphics specifications
\usepackage{wrapfig}
\usepackage[pdftex]{graphicx}      % alternative graphics specifications
\usepackage{longtable}     % helps with long table options
\usepackage{epsf}          % old package handles encapsulated post script issues
\usepackage{bm}            % special 'bold-math' package
\usepackage{verbatim}       % for comment environment
\usepackage[colorlinks=true]{hyperref}  % this package should be added after all others
                                        % use as follows: \url{http://web.mit.edu/8.13}

%\addtolength\topmargin{-.5\topmargin} %cuts the top margin in half.

%
% And now, begin the document...
% Students should not have to alter anything above this line
%

\begin{document}
\title{Chaotic Pendulum}
\author         {Samuel Wallace and Sophia Bergmann}
\email          {swallace6859@ufl.edu}
\date{\today}
\affiliation{University of Florida Department of Physics}


\begin{abstract}

A spring and pulley apparatus is measured to determine its properties as a dynamical system. As a nonlinear system with relatively few degrees of freedom, features and quantities associated with deterministic chaos can be accurately measured and determined to high precision, making it an excellent basis for understanding more complicated dynamical systems. A rotary encoder measured angular displacement data, which was processed into plots of Poincare Sections and bifurcation diagrams. These plots were then analyzed for important dynamical quantites, such as Lyapunov exponents and box dimension of the strange attractor. Lyapunov exponents $-0.048$ and a box dimension of $1.52$ in a highly damped experimental run shows the system meets the general criteria for deterministic chaos.

\end{abstract}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction and Theory\cite{strogatz}\cite{moon}\cite{baker-gollub}\cite{arbarbanel}}

\indent Deterministic chaos in classical systems is a new topic in physics, engineering, and mathematics describing the phenomena of a system and its trajectories, completely determined by initial conditions, showing unexpected or unpredictable response as the trajectories move forward in time. The initial discovery of deterministic chaos by Edward Lorenz has led to the rise of a branch of applied math and sciences known as "Dynamical Systems." \\
\indent Though there is no rigorous and universal definition of chaos, there are three theoretical criteria that are widely accepted:
\begin{enumerate}
    \item Perturbations to inital conditions must show dramatic changes in how trajectories evolve,
    \item Trajectories must evolve regions of phase space through "nearly all" of the entire phase space
    \item There are trajectories which explore "nearly all" of phase space.
\end{enumerate}
The phrase "nearly all" can be made rigorous in the mathematical terminology of topology; confer with \ref{}. These exact conditions are not often directly worked with, as there is a feature of phase space that can satisfy these criteria: a strange attractor. A strange attractor is an region of phase space into which trajectories eventually enter and remain in, and from which quantities can be calculated that measure the chaotic tendency of the system. \\
\indent Strange attractors make measuring chaos easy, as a bounded region of phase space is easier to take measurements of than the entire unbounded space. However, strange attractors live up to their name: their geometric structure is complex. Strange attractors form highly irregular shapes, with a topological dimension somewhere between 1 and 2. However, criteria 2 and 3 from above make determining the shape of a chaotic attractor simpler: since many trajectories explore "nearly all" of the chaotic attractor, so following a single trajectory can map out much of an attractor. Thus a single initial condition can shed light into the chaotic properties of a system. \\
\indent Criterion 1 is rather easily quantified through Lyapunov exponents. Lyapunov exponents measure how quickly nearby trajectories spread out. Two nearby initial conditions are evolved forward in time by the system, and their distance is fitted to an exponential function (possible growth or decay), and the growth constant of the exponential function is named the Lyapunov exponent. Negative exponents means that solutions approach each other, positive indicates the solutions diverge, and zero indicates roughly constant distance between the solutions. \\
\indent There is also another quantity relevant to the study of a strange attractor: the fractal dimension of the attractor. Since solutions spread out inside the attractor, the more compactified the attractor is (i.e. smaller dimension), a single trajectory will explore the attractor more quickly and more fully. There are a variety of ways for calculating dimension; the one we will use is the box dimension, which how many boxes of a grid are filled by the attractor. As the grid size expands over the whole attractor, an overall measure of the dimension of the attractor is obtained. \\
\section{Apparatus and Experiment}
\indent The apparatus for the experiment is a simple pendulum, attached to two springs, the end of one of which is fixed, and the other is connected to a motor driver. The set up is identical to that shown in Figure \ref{fig:pend-diag}.


The equations of motion for the system are

\[\dot{\theta} = \omega\]
\[\dot{\omega} = - \Gamma \omega - \Gamma' \mathrm{sgn}\omega - \kappa \theta + \mu \sin \theta + \epsilon \cos \phi\]
\[\dot{\phi} = \Omega\]

The derivation of these equations is included in Appendix \ref{sec:deriv}. \\
\indent The axle of the pendulum is mounted on a Pasco Rotary Motion Sensor to record angular displacement. A National Instruments PCI 6601 timer/counter board is used to both collect data from the rotary encoder and to drive the stepper motor. Several programs in LabView were used to process, save, and display recorded data. \\
\indent The rotary encoder records the position 200 times in the span of a drive period. Savitsky-Golay Filtering was used in the LabView program to numerically interpolate the angular acceleration of the pendulum. \\
\indent A table of the measured masses and dimensions of the apparatus are included in Table \ref{tab:quants}.


\begin{table}[h]
    \centering
    \begin{tabular}{c|c}
        Quantity & Value \\ \hline \hline
        Aluminum Disk mass & 127.2 $\pm$ 0.05 g \\
        Aluminum Disk diameter & 9.52 $\pm$ 0.005 cm \\
        Pendulum weight mass & 14.4 $\pm$ 0.05 g \\
        Pulley diameter & 2.86 $\pm$ 0.005 cm \\
        Pendulum weight mounting distance & 4.57 $\pm$ 0.005 cm \\
        Spring constant & 17.6 $\pm$ 0.005 N/m \\
        \hline \hline
    \end{tabular}
    \caption{Dimensions of various parts of the chaotic pendulum}
    \label{tab:quants}
\end{table}


\begin{figure}[h]
    \centering
    \includegraphics[scale=0.45]{pendulum-diagram.png}
    \caption{The Chaotic Pendulum apparatus (from \cite{deserio})}
    \label{fig:pend-diag}
\end{figure}



\section{Analysis and Results}
\indent Several measurements were planned for the experiment. As preliminary measurements, the pendulum mass was removed to linearize the system, then the pendulum mass was restored and at low amplitude, the driving frequency was increased and decreased. Finally, the driving amplitude was increased with the intent of observing chaotic response. In addition, data was collected with the intent of building a bifurcation diagram of the system, the results of which are discussed in Appendex \ref{sec:bifurcation}\\
\subsection{Oscillatory Behavior}
\indent In the linear regime (i.e. without the pendulum mass) the equations of motion reduce to a driven-damped oscillator, so the amplitude response of the pendulum should show the classical peak at resonance. The response of the pendulum at different frequencies is shown in Figure \ref{fig:lin-resp}. The amplitude response shows a clear increase near the resonance frequency.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{lin-resp.png}
    \caption{Angular Displacement of the Linearized System versus scaled time at different driving frequencies. The driving amplitude is $A=2.5cm$.}
    \label{fig:lin-resp}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{nonlin-resp.png}
    \caption{The Amplitude Response of the nonlinear system. The driving amplitude is $A=2.5cm$.}
    \label{fig:nonlin}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{limit-cycle.png}
    \caption{Caption}
    \label{fig:limit-cycle}
\end{figure}

\indent For the small amplitude nonlinear oscillation, the amplitude is shown in figure \ref{fig:nonlin}. A hysteretic response is observed; this is highly indicative of the strong effect of the nonlinearity.Several attracting limit cycles were found, a plot of the large limit cycle and one of the small limit cycles (there is another symmetric to it); these are shown in figure \ref{fig:limit-cycle}


\subsection{Chaotic Response}

Next the amplitude was increased to $A=6cm$, the drive frequency was set to near resonance $f=0.825$Hz, and the magnet was moved to 5mm from the disk (high damping). The experiment ran for 2000 drive cycles. A Poincare plot was then generated. It is shown in figure \ref{fig:high-damp-poincare}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{poincare-real-high-damping.png}
    \caption{A Poincare Section of the highly damped chaotic pendulum}
    \label{fig:high-damp-poincare}
\end{figure}


\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{high-damping-sim.png}
    \caption{Poincare Section of a higly damped Simuated Trajectory}
    \label{fig:sim-poincare-high-damping}
\end{figure}

Using a LabView Program, the pendulum system was simuated. A Poincare section of the simulated trajectory is shown in Figure \ref{fig:sim-poincare-high-damping}.\\
\indent Lyapunov exponents were calculated for the simulated and experimental results. They are shown in table \ref{tab:lyapunov-high}. There is an order of magnitude difference between the experimental and simulated data's Lyapunov exponents. The cause for the difference is currently unknown. The box dimension of the fractals were calculated. The experimental data showed a box dimension of 1.5176, and the simulated data gave 1.4752.\\


\begin{table}[h]
    \centering
    \begin{tabular}{ccc}
         Run & Lyapunov Exponent (x) & Lyanpunov Exponent (y) \\ \hline \hline
         Experimental & 0.179 & -0.227 \\
         Simulated & 1.32 & -1.6 \\
        \hline \hline
    \end{tabular}
    \caption{Lyapunov Exponents for High Damping}
    \label{tab:lyapunov-high}
\end{table}


\indent Next the damping magnet was retracted, and data for a low-damping run was collected. Poincare sections for experimental and simulated data are shown in figures \ref{fig:low-damp-poincare} and \ref{fig:low-poincare-sim}, respectively. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{low-damp-poincare-section.png}
    \caption{Experimental Poincare Section of a Low-Damping Run}
    \label{fig:low-damp-poincare}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{low-damp-sim-poincare-section.png}
    \caption{Low Damping Poincare Section}
    \label{fig:low-poincare-sim}
\end{figure}

The Lyapunov Exponents for low damping are shown in table \ref{tab:lyapunov-low}. The box dimension of the experimental data evaluated to 1.7877, and the simulated data had a dimension of 1.7589. 


\begin{table}[h]
    \centering
    \begin{tabular}{ccc}
         Run & Lyapunov Exponent (x) & Lyanpunov Exponent (y) \\ \hline \hline
         Experimental & 0.070 & -0.09 \\
         Simulated & 1.35 & -1.35 \\
        \hline \hline
    \end{tabular}
    \caption{Lyapunov Exponents for Low Damping}
    \label{tab:lyapunov-low}
\end{table}







\section{Conclusions}

\indent The results are a mix of agreements and disagreements with theory. One one hand, the box dimension shows that increased damping decreases the box dimension of the fractal, as expected, but Lyapunov exponents of the simulation and experimental data varied  significantly. Thus large-scale properties of the attractor show good agreement with theory, but small scale properties of the attractor show significant variance.\\
\indent Perhaps the effect of chaos in the system is too dynamic and too greatly varying to expect more than slight agreement. Round-off error, numerical artifacts, and imperfect algorithms may so dramatically effect the shape of the attractor in a way that a real system would not see. \\
\indent There are still more quantities from the mathematical theory of dynamical systems that can be used to analyze the response of the pendulum. More extensive bifurcation diagrams can be constructed and analyzed. There are also more chaotic systems that can be analyzed for their chaotic properties in a laboratory setting; for example, see \cite{strogatz} for how to set up a circuit that carries out Lorenz's original system. 




\bibliography{sample-paper}

\appendix
\section{Derivation of the Equations of Motion}
\label{sec:deriv}
Throughout, $I$ is the moment of intertia of the disk, $b$ the damping coefficient due to the magnet, $b'$ the damping coefficient due to axle friction, $k$ the spring constant of the springs, $r$ the radius of the disk, $m$ the mass of the pendulum mass, $l$ the distance from the mass to the axis of rotation, and $A$ the amplitude of the driving motor.

The equations of motion arise from Newton's Second Law in rotational form:
\[\tau_{ext}= I \dot{\omega}    \]
We now need to determine the total torque. Each spring contributes a Hooke's Law force, the pendulum contributes a gravitational force \`a la simple pendulum, and the motor contributes to spring expansion. The conversion to torque gives the following terms (in magnitude):
\[ \tau_{spring} = 2 k r \Delta s \]
\[  \tau_{gravity} = mgl \sin \theta \]
\[ \tau_{motor} = kd r \]
Where $k$ is the spring constant, $r$ is the disk radius, $m$ is the mass of the pendulum mass, $h$ is the height of the pendulum mass, $\Delta s$ is the arc length traced out by the pendulum mass. Note that the $\sin \theta$ is included as only the angular component of gravity acts on the mass. Including the following relation:

\[\Delta s = r \theta\]

Taking the counterclockwise torque as positive, we see that the total torque (incuding both of the two springs) is 
\[
\tau_c = -2 kr^2 \theta + mgl \sin \theta + krd
\]



Substituting in the total torque to Newton's Second Law, with the addition of the friction terms, we see that:
\[I \dot{\omega} = -b \omega - b' \mathrm{sgn}{\omega} - 2kr^2 \theta \]
\[ + mgl \sin \theta + krA \cos (\omega t) \]
Now if we define $\phi = \Omega t$, recognize that $\dot{\theta}=\omega$, and divide through by $I$, we can rewrite the equation as a set of three equations: 

\[\dot{\theta}=\omega\]
\[\dot{\omega} = -\Gamma \omega - \Gamma ' \mathrm{sgn} \omega \]
\[ - \kappa \theta + \mu \sin \theta + \epsilon \cos \phi\]
\[\dot{\phi} = \Omega\]

\section{Bifurcation Diagram}
\label{sec:bifurcation}
\indent A Bifurcation Diagram records stable positions in phase space vs a parameter of the differential equation. They are particularly useful for mapping out different qualitative features of dynamical systems. In particular, limit cycles and chaotic behavior can be seen as the number of stable positions increase. \\
\indent A bifurcation diagram was constructed using the LabView programs by collecting a data point once a drive cycle (by setting sections/cycle = 1) and using the stepper motor controls to increase the frequency after a set amount of drive cycles. \\
\indent We stepped the frequency from $0.45$Hz to $0.945$Hz in increments of $0.005$Hz, incrementing every 20 drive cycles. The driving amplitude was set $A=6cm$, and the damping magnet was placed at a $5$mm distance from the pendulum disk. The resulting diagram was then created:

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{bifurcation.png}
    \caption{Equilibrium positions versus driving frequency.}
    \label{fig:bifurcation}
\end{figure}

This diagram was deemed not clean enough to include in the main report, though onset of chaos is clearly visible at roughly 0.725Hz. It is left in the appendix as a suggestion to future researchers on possible extensions of the experiment. It is noted in the lab write-up that there is no solid method for determining the conditions of higher frequency limit cycles. Perhaps by creating more detailed and higher resolution bifurcation diagrams, the conditions for these limit cycles can be accurately determined.


\end{document}
