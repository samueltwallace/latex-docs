\documentclass{article}

\usepackage{amsmath} 
\usepackage{amsfonts} 
\usepackage{mathrsfs} 
\usepackage{amssymb} 
\usepackage{amsthm} 
\newtheorem{thm}{Theorem}
\newtheorem{defn}{Defintion}
\newtheorem*{pf}{Proof}
\newtheorem*{ex}{ $ \mathbb{E} $}
\newtheorem*{rmk}{Remark}
\newtheorem*{claim}{Claim}
\newtheorem{cor}{Corollary}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\title{MAA 4227 Measure Theory}
\author{Samuel Wallace}
\date{\today}

\begin{document}
\maketitle

\begin{defn}
	The \textbf{Lebesgue Outer Measure} is the function $ \lambda^{*} : \mathbb{P}( \mathbb{R}) \to \left[ 0, \infty \right] $ by the rule 
	\[
		\lambda^{*} (A) = \inf \sum_{n} \vert I_{n} \vert
	\]
	where the infimum is taken over all countable covers of $ A $ by open subsets of $ \mathbb{R} $.
\end{defn}

\begin{prop}
	The following are true about $ \lambda^{*} $:
	\begin{enumerate}
		\item $ \lambda^{*}( \emptyset) = 0 $ 
		\item $ A \subset B \Rightarrow \lambda^{*}(A) \leq \lambda^{*}(B) $
		\item $ \lambda^{*} \left( \bigcup_{n} A_{n} \right) \leq \sum_{n} \lambda^{*} (A_{n}) $ 
	\end{enumerate}
\end{prop}

\begin{thm}
	If $ I $ is an interval, then $ \lambda^{*}(I) = \vert I \vert $.
\end{thm}

\begin{pf}
	{First,} let $ I = [a,b] $ be compact. Clearly $ \lambda^{*} (I) \leq \vert I \vert $. Now let $ I_{0} = (a- \epsilon, b+ \epsilon) $ and let $ I_{n} = (0, \frac{3}{2^{n}}  $; clearly $ I \subset \cup I_{n}$ and $ \sum_{n} \vert I_{n} \vert = b - a + 3 \epsilon $. \medskip \\
	Now let $ \mathcal{J} $ be a countable collection of bounded open intervals covering $ [a,b] $. By compactness, there are $ (a_{1}, b_{1}) \ldots (a_{N}, b_{N}) \in \mathcal{J} $ that cover $ [a,b] $. \medskip \\

	\begin{claim}
		$ b-a \leq \sum_{n} (b_{n} - a_{n}) $
	\end{claim}

	It follows that $ b-a \leq \sum_{n} \vert (a_{n}, b_{n} \vert \leq \sum_{J \in \mathcal{J}} \vert J \vert $, which makes $ b-a $ a lower bound, which makes $ \lambda^{*} [a,b] $ greater, so that $ b-a \leq \lambda^{*} [a,b] $. \medskip \\
	Next, let $ I \subset \mathbb{R} $ be any bounded interval. Then $ I \subset \overline{I} $ which is compact so that $ \lambda^{*}(I) \leq \lambda^{*}( \overline{I}) = \vert \overline{I} \vert = \vert I \vert $; while $ I $ contains compact intervals $ J $ with $ \lambda^{*}(I) \geq \vert I \vert $.
\end{pf}


\begin{defn}
A subset $ A \subset \mathbb{R} $ is \textbf{Lebesgue measurable} when it satisfies
\[
	\forall B \subset \mathbb{R} \hspace{4pt} \lambda^{*} (B \cap A) + \lambda^{*} (B \cap A^{c})
\]
which is to say, $ A $ and its complement break up every $ B \subset \mathbb{R} $ into two pieces on which $ \lambda^{*} $ adds properly.
\end{defn}

\begin{defn}
We will use $ \mathcal{M}_{ \lambda^{*}} $ to be the set of all Lebesgue measurable subsets of $ \mathbb{R} $.
\end{defn}
\begin{thm}
	If $ \lambda^{*}(A) = 0$ or $ \lambda^{*}(A^{c}) =0 $, then $ A \in \mathcal{M}_{ \lambda^{*}} $.
\end{thm}

\begin{defn}
	Let $ \mu^{*}: \mathbb{P}( \Omega) \to [0, \infty] $ be an outer measure. We call $ A \subset \Omega $ $ \mu^{*}$-measurable (or just measurable) if
	\[
		\forall B \subset \Omega \hspace{4pt} \mu^{*}(B) = \mu^{*}(B \cap A) + \mu^{*}(B \cap A^{c})
	\]
	
\end{defn}

We now look to make a `true' measure from an outer measure. Let $ \mu^{*}: \mathbb{P}( \Omega) \to [0, \infty] $ be an arbitrary outer measure; thus,
\begin{enumerate}
	\item $ \mu^{*}( \emptyset) = 0 $
	\item $ A \subset B \Rightarrow \mu^{*}(A) \leq \mu^{*}(B)$
	\item $ \mu^{*}( \cup_{n} A_{n}) \leq \sum_{n} \mu^{*}\left( A_{n} \right)  $
\end{enumerate}
And as before, $ \mathcal{M}_{ \mu^{*}} $ is the set of $ \mu^{*} $-measurable subsets of $ \Omega $.


\begin{thm}
If $ \left\{ A_{n} \right\} \subset \mathcal{M}_{ \mu^{*}} $, then $ \cup_{n} A_{n} in \mathcal{M}_{ \mu^{*}}$.

\end{thm}
\begin{claim}
	$ \mathcal{M}_{ \mu^{*}} $ is closed under \textbf{finite} unions.
\end{claim}

\begin{pf}
\[
	\mu^{*}\left( B \cap \left( A_{0} \cup A_{1} \right) \right) + \mu^{*}\left( B \cap \left( A_{0} \cup A_{1} \right)^{c} \right)
\]
\[
	= \mu^{*}\left( B \cap \left( A_{0} \cup A_{1} \right) \cap A_{0} \right) + \mu^{*}\left( B \cap \left( A_{0} \cup A_{1} \right) \cap A_{0}^{c} \right) + \mu^{*} \left( B \cap A^{c}_{0} \cap A^{c}_{1} \right)	
\]
\[
	=\mu^{*} \left( B \cap A_{0} \right) + \mu^{*} \left( B \cap A_{0}^{c} \cap A_{1} \right) + \mu^{*} \left( B \cap A_{0}^{c} \cap A^{c}_{1} \right)
\]
\[
	= \mu^{*} \left( B \cap A_{0} \right) + \mu^{*}(B \cap A_{0}^{c}) = \mu^{*}(B)
\]

\end{pf}

\begin{claim}
$ \mathcal{M}_{ \mu^{*}} $ is closed under countable \textbf{disjoint} union.
\end{claim}

\begin{proof}
	Let $ \left\{ A_{n} \right\} $ be (pairwise) disjoint. Let $ A = \cup A_{n} $ and $ A^{N} = \cup_{n}^{N A_{n}} $ and $ B \subset \mathbb{R} $ be arbitrary.

	\begin{claim}
		If $ N \geq 0 $ then 
		\[
			\mu^{*} \left( B \cap A^{N} \right) = \sum_{n}^{N} \mu^{*} \left( B \cap A_{n} \right)
		\]
		
	\end{claim}

	\begin{pf}
	\[
		\mu^{*} \left( B \cap A^{N+1} \right) = \mu^{*} \left( B \cap A^{N+1} \cap A^{N} \right) + \mu^{*} \left( B \cap A^{N+1} \cap \left( A^{N} \right)^{c} \right)
	\]
	\[
		\mu^{*}\left( B \cap A^{N} \right) + \mu^{*}\left( B \cap A_{N+1} \right)
	\]
	\[
		= \sum_{n}^{N} \mu^{*}\left( B \cap A_{n} \right) + \mu^{*}\left( B \cap A_{N+1} \right)
	\]
	
	
	\end{pf}
Since $ \mu^{*} $ is montonic, it follows that 
\[
	\mu^{*}\left( B \cap A \right) \geq \sum_{n}^{N} \mu^{*}\left( B \cap A_{n} \right)
\]
So
\[
	\mu^{*}\left( B \cap A \right) \geq \sum_{n}\left( B \cap A_{n} \right)
\]
Now
\[
	\mu^{*}(B) = \mu^{*}\left( B \cap A^{N} \right) + \mu^{*}\left( B \cap \left( A^{N} \right)^{c} \right)
\]
\[
	= \sum_{n}^{N} \mu^{*}\left( B \cap A_{n} \right) + \mu^{*}(B \cap \left( A^{N} \right)^{c})
\]
\[
	\geq \sum_{n}^{N} \mu^{*}(B \cap A_{n}) + \mu^{*}\left( B \cap A^{c} \right)
\]
so that
\[
	\mu^{*}\left( B \right) \geq \mu^{*}\left( B \cap A \right) + \mu^{*}\left( B \cap A^{c} \right)
\]
So that can be combined with the previous inequality and obtain equality, so that $ A = \cup_{n}A_{n} $ is in $ \mathcal{M}_{ \mu^{*}} $.

\begin{claim}
	Let $ \left\{ A_{n} \right\} \subset \mathcal{M}_{ \mu^{*}} $. Let $ \Tilde{A}_{0} A_{0}$, and let $ \Tilde{A}_{n} = A_{n} \backslash \left( A_{0} \cup \cdots \cup A_{n-1} \right) $ Then
	\[
	\bigcup_{n} A_{n} = \bigcup_{n} \Tilde{A}_{n}
	\]
	So that $ \Tilde{A}_{n} \in \mathcal{M}_{ \mu^{*}} $ and are pairwise disjoint. 
\end{claim}

	
\end{proof}

\begin{thm}
	If the countable collection $ \left\{ A_{n} \right\} $ of measurable sets is \textbf{pairwise disjoint}, then 
	\[
		\mu^{*}\left( \cup_{n}A_{n} \right) = \sum_{n} \mu^{*}\left( A_{n} \right)
	\]
	
\end{thm}

\begin{pf}
Since
\[
	\mu^{*}\left( B \cap A \right) = \sum_{n} \mu^{*}\left( B \cap A_{n} \right)
\]
And let $ B = \Omega $.
\end{pf}

\begin{defn}
	A \textbf{ $ \sigma $-algebra} on the set $ \Omega $ is a collection $ \mathcal{A} \subset \mathbb{P}( \Omega) $ of subsets of $ \Omega $ satisfying
	\begin{enumerate}
		\item $ \mathcal{A} $ is closed under complementation
		\item $ \mathcal{A} $ is closed under countable unions
		\item $ \mathcal{A} $ ic closed under countable intersections
		\item $ \mathcal{A} $ contains $ \emptyset $ and $ \Omega $
	\end{enumerate}
	A set with a $ \sigma $-algebra is often called a `measurable space,' the sets in the $ \sigma $-algebra being called the `measurable' subsets of $ \Omega $.
\end{defn}

\begin{defn}
	A \textbf{measure} on the measurable space $ \left( \Omega, \mathcal{A} \right) $ is a function
	\[
		\mu: \mathcal{A} \to [0, \infty]
	\]
	such that $ \mu( \emptyset) = 0 $ and for pairwise disjoint sets $ \left\{ A_{n} \right\} $,
	\[
		\mu\left( \bigcup_{n}A_{n} \right) = \sum_{n} \mu\left( A_{n} \right)
	\]
	A \textbf{measure space} is a set $ \Omega $ with a preferred $ \sigma $-algebra and a preferred measure $ \mu: \mathcal{A} \to [0, \infty] $.	
	
\end{defn}

\begin{thm}
	If $ \mu^{*}: \mathbb{P}( \Omega) \to [0, \infty] $ is an outer measure, then
	\begin{enumerate}
		\item $ \mathcal{M}_{ \mu^{*}} \subset \mathbb{P}\left( \Omega \right) $ is a $ \sigma $-algbra on $ \Omega $ 
		\item The restriction $ \mu = \mu^{*} \restriction_{ \mathcal{M}_{ \mu^{*}}} $  
	\end{enumerate}
\end{thm}

\begin{cor}
The \textbf{Lebesgue measurable} sets constitute a $ \sigma $-algebra $ \mathcal{M}_{ \lambda^{*}} $ on which the outer measure $ \lambda^{*} $ restricts to define the \textbf{Lebesuge measure} $ \lambda $.
\end{cor}

\begin{defn}
	A measure space $ \left( \Omega, \mathcal{A}, \mu \right) $ is called \textbf{complete} iff
	\[
		B \subset A \in \mathcal{A} \hspace{4pt} \& \hspace{4pt} \mu(A) = 0 \Rightarrow B \in \mathcal{A}.	
	\]
	
\end{defn}

\begin{thm}
	Each interval of the form $ [a, \infty) $ is Lebesgue measurable. (Proof yet to be seen).
\end{thm}

\begin{defn}
	Let $ \mathcal{C} \subset \mathbb{P}( \Omega) $ be a collectio of subsets of $ \Omega $. The $ \sigma $-algebra \textbf{generated} by $ \mathcal{C} $ is the smallest $ \sigma $-algebra that contains $ \mathcal{C} $.
\end{defn}

\begin{defn}
Let $ \Omega $ be a metric or topological space. The \textbf{Borel} $ \sigma $-algebra is the $ \sigma $-algebra generated by the open subsets of $ \Omega $.
\end{defn}

\begin{rmk}
	$ B( \mathbb{R}) $ is generated by any and all of these families of open sets:
	\begin{enumerate}
		\item $ \left\{ \text{all open sets} \right\} $ 
		\item $ \left\{ \text{all open intervals} \right\} $ 
		\item $ \left\{ \text{all } (a,b)s \right\} $ 
		\item $ \left\{ \text{all } [a,b)s \right\} $ 
		\item $ \left\{ \text{all } [a, \infty)s \right\} $ 
	\end{enumerate}
\end{rmk}

\begin{thm}
	$ \mathcal{M}_{ \mu^{*}} \supset B( \mathbb{R}) $ 
\end{thm}

\begin{defn}
	A function $ f: \Omega \to \Omega' $ between measure spaces is called \textbf{measurable} if for every measurable subset $ B \subset \Omega' $, the set $ f^{-1}\left( B \right) $ is a measurable set in $ \Omega $.
\end{defn}

\begin{thm}
	Let $ \mathcal{A}' = \sigma\left( \mathcal{C}' \right) $ . For $ f: \Omega \to \Omega' $ the following are equivalent:
	\begin{itemize}
		\item[ $ \mathcal{A}' $ ] $ \forall B \in \mathcal{A}' \hspace{4pt} f^{-1}(B) $  is measurable in $ \Omega $ .
		\item[ $ \mathcal{C}' $ ] $  \forall B \in \mathcal{C}' \hspace{4pt} f^{-1}(B) $ is measurable in $ \Omega $ .
	\end{itemize}
	
\end{thm}

\begin{thm}
	For $ f: \Omega \to [- \infty, \infty]  $ TFAE:
	\begin{enumerate}
		\item $ f $ is measurable
		\item $ \forall c \in \mathbb{R} \hspace{4pt} \left\{ f \leq c \right\} $ is measurable
		\item $ \forall c \in \mathbb{R} \hspace{4pt} \left\{ f > c \right\} $ is measurable
		\item $ \forall c \in \mathbb{R} \left\{ f \geq c \right\}$ is measurable
		\item $ \forall c \in \mathbb{R} \hspace{4pt} \left\{ f < c \right\} $ is measurable
	\end{enumerate}
\end{thm}






\end{document}

