
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sample template for MIT Junior Lab Student Written Summaries
% Available from http://web.mit.edu/8.13/www/Samplepaper/sample-paper.tex
% Last Updated April 12, 2007
%  Edited by Robert DeSerio August 2010.
%
% Adapted from the American Physical Societies REVTeK-4 Pages
% at http://publish.aps.org
%
% ADVICE TO STUDENTS: Each time you write a paper, start with this
%    template and save under a new filename.  If convenient, don't
%    erase unneeded lines, just comment them out.  Often, they
%    will be useful containers for information.
%
% Using pdflatex, images must be either PNG, GIF, JPEG or PDF.
%     Turn eps to pdf using epstopdf.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREAMBLE

\documentclass[aps,twocolumn,secnumarabic,balancelastpage,amsmath,amssymb,nofootinbib,hyperref=pdftex]{revtex4}

% Documentclass Options
% aps, prl, rmp stand for American Physical Society, Physical Review Letters, and Reviews of Modern Physics, respectively
% twocolumn permits two columns, of course
% nobalancelastpage doesn't attempt to equalize the lengths of the two columns on the last page
% as might be desired in a journal where articles follow one another closely
% amsmath and amssymb are necessary for the subequations environment among others
% secnumarabic identifies sections by number to aid electronic review and commentary.
% nofootinbib forces footnotes to occur on the page where they are first referenced
% and not in the bibliography
% REVTeX 4 is a set of macro packages designed to be used with LaTeX 2e.
% REVTeX is well-suited for preparing manuscripts for submission to APS journals.


\usepackage{chapterbib}    % allows a bibliography for each chapter (each labguide has it's own)
\usepackage{color}         % produces boxes or entire pages with colored backgrounds
\usepackage{graphics}      % standard graphics specifications
\usepackage[pdftex]{graphicx}      % alternative graphics specifications
\usepackage{longtable}     % helps with long table options
\usepackage{epsf}          % old package handles encapsulated post script issues
\usepackage{bm}            % special 'bold-math' package
\usepackage{verbatim}			% for comment environment
\usepackage[colorlinks=true]{hyperref}  % this package should be added after all others
% use as follows: \url{http://web.mit.edu/8.13}

%\addtolength\topmargin{-.5\topmargin} %cuts the top margin in half.

%
% And now, begin the document...
% Students should not have to alter anything above this line
%



\begin{document}
\author{Samuel Wallace and Jason Pioquinto}
\title{Determining Particulate Size by Dynamic Light Scattering}
\email{swallace6859@ufl.edu}
\date{\today}
\affiliation{University of Florida Department of Physics}



\begin{abstract}
	Dynamic Light Scattering with a HeNe laser on polymer beads in water is used to determine the diameter of the spheres. Data analysis based on the Fourier transform of scattered wave intensity versus time is used to determine the autocorrelation function and power spectrum of the intensity of the scattered light. Quantities from the expected shape of the curve of the power spectrum and autocorrelation function relate to relevant quantities about the ambient environment and properties of the spheres. Manufacturer-reported $ 0.5 \mu $m diameter spheres were measured to have a diameter of $ 0.486 \pm 0.07 \mu $m, and manufacturer-reported $ 1.0 \mu $m diameter spheres were measured to have diameters of $ 1.217 \pm 0.2 \mu $m. Extra data quality information was determined by not forcing a linear regression through the origin, though theory suggested that there was a zero $ y $-intercept.
\end{abstract}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
\indent This section draws heavily on the presentation in \cite{dlslab}, \cite{benedek1970}, and \cite{gillespie1996}. \\
\indent Scattering experiments are powerful methods for analyzing properties of matter. Dynamic light scattering is a type of scattering experiment using visible light to track the movement of suspended particles in a fluid by analysis of the intensity of scattered light versus time. We will briefly outline the mechanism and data analysis steps involved in dynamic light scattering. \\
\indent Brownian motion is the semi-random movement of individual particles when immersed in a liquid or gas. The motion is mathematically described by a probability distribution that evolves in time according to a partial differential equation, the \textit{diffusion equation}:
\begin{equation}
	\frac{ \partial \rho}{\partial t } = D \nabla^2 \rho
\end{equation}
Where $ \rho = \rho (x,y,z,t)$ is the particle density in space and time, and $D$ is the diffusion diffusion coefficient, which for particles in water, is
\begin{equation}
D = \frac{k_B T}{3 \pi \eta d} 
\end{equation}
which is called the \textit{Stokes-Einstein relation}.In the relation, $k_{B}$ is the Boltzmann constant, $T$ is the absolute ambient temperature, $ \eta$ is the viscosity of water (at the ambient temperature), and $d$ is the diameter of the particle.\\
\indent An incident electromagnetic wave will scatter off a particle immersed in the liquid, creating a scattered wave that will interfere with the incident wave, creating an interference pattern. This interference pattern then impinges on the detector. Two types of data analysis are implemented: a Fourier transform and an autocorrelation function.\\
\indent A Fourier transform gives the power spectrum of the incident wave in time; it measures the frequency components of the intensity on the detector versus time. The power spectrum measures the intensity of different frequency components. It is defined by the formula
\begin{equation}
	S_I ( \omega) = \lim_{ T \to \infty } \frac{1}{2T} \widehat{I}( \omega) \widehat{I}^* ( \omega) 
\end{equation}
where
\[
	\widehat{I}( \omega) = \int_{ - \infty }^{ \infty } I(t) e^{ i \omega t }dt 
\]
is the Fourier transform of the funcion $I(t)$, the intensity as a function of time.\\
\indent The autocorrelation of a signal $I(t)$ is defined as
\begin{equation}
	R_I ( \Delta t) = \lim_{ T \to \infty } \frac{1}{2T} \int_{ -T }^{ T } I(t) I(t + \Delta t) dt
\end{equation}
For the diffusion equation, the power spectrum takes the form
\begin{equation}
	S_I( \omega) = A \delta( \omega) + \frac{B}{1+ ( \omega / \Gamma)^{ 2 }} 
	\label{eq:pow-spec}
\end{equation}
For constants $A,B$, and $ \Gamma$ is given by
\begin{equation}
	\Gamma =  \kappa \frac{\sin ^2 ( \theta / 2)}{d} 
	\label{eqn:gamma}
\end{equation}
where $ \kappa$ is a combination of physical constants, $ \theta$ is the scattering angle, and $d$ is the diameter of the particle. The autocorrelation function for the diffusion equation is calculated to be
\begin{equation}
	R_I ( \Delta t) = A' + B' e^{ - \Gamma \Delta t }
	\label{eq:auto-corr}
\end{equation}
where $A'$ and $B'$ are constants and $ \Gamma$ is the same as before.\\
\indent By calculating the autocorrelation function and power spectrum of measured intensity data, we can determine $ \Gamma$, which will in turn gives the diameter of the particle. In the next section we will describe an apparatus for using the above calculations to determine the size of polymer spheres suspended in water.



\section{Experiment}
\indent The experimental setup, in brief, is as follows: a HeNe laser shines through a cell of suspended polymer beads in water. A photodetector, offset from the direction of the laser, detects the intensity of scattered light rays and sends the data to a data acquisition unit, which then sends data through a LabView program. Then the data is collected for processing. The physical setup is shown in Figure~\ref{fig:expt-setup}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{img/expt-setup.png}
	\caption{The arrangement of laster, scattering cell, and photodetector. \cite{dlslab}}%
	\label{fig:expt-setup}
\end{figure}

\indent The photodetector is a silicon avalance photodiode circuit. Incident light on the surface of the detector causes a proportional current to flow, which is then attenuated by a Hamamatsu S2382 detector. A schematic for the detector is shown in Figure~\ref{fig:circuit-diag}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{img/circuit-diag.png}
	\caption{A circuit schematic for the Hamamatsu S2382. \cite{dlslab}}%
	\label{fig:circuit-diag}
\end{figure}
\indent The data processing is then done in LabView to fit the theoretical expectations for the power spectrum and autocorrelation function (Equations \ref{eq:pow-spec} and \ref{eq:auto-corr}).

\section{Results}
\indent A sample autocorrelation function as plotted in the LabView program is shown in Figure \ref{fig:auto-corr}. The exponential decay is evident. The fitting factors and their uncertainty is also calculated in the LabView program.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{img/auto-corr.png}
	\caption{An Example Autocorrelation Function with Fit}
	\label{fig:auto-corr}
\end{figure}

\indent Values of $ \Gamma $ were collected at different angles for two different sphere sizes. The size of spheres, as reported by the manufacturer, were 0.5 $ \mu $ m and 1.0 $ \mu $ m. A table of the data is shown in Table \ref{tab:gamma-d-0.5}. According to Equation \ref{eqn:gamma}, this should be a linear relation. A plot of the data is shown in Figure \ref{fig:gamma-plot-0.5}. The linear regression had a $ \chi^{2}_{ \nu} = 1.3$.

\begin{table}[h]
	\centering
	\caption{$ \Gamma$ vs $ \sin^{2}( \theta) $ for the half-micron sphere from the power spectrum and autocorrelation function.}
	\label{tab:gamma-d-0.5}
	\begin{tabular}{ccc}
		$ \Gamma $ from power & $ \Gamma $ from autocorrelation & $ \sin^{2}( \theta ) $ \\
		\hline \hline
		2.24 $ \times 10^{2} $ & $ 2.22 \times 10^{2} $ & 0.093\\
		$ 2.57 \times 10^{2} $ & $ 2.51 \times 10^{2} $ & 0.146\\
		$ 2.86 \times 10^{2} $ & $ 2.80 \times 10^{2} $ & 0.235\\
		$ 3.39 \times 10^{2} $ & $ 3.34 \times 10^{2} $ & 0.324\\
		$ 4.69 \times 10^{2} $ & $ 4.65 \times 10^{2} $ & 0.444\\
		$ 6.38 \times 10^{2} $ & $ 6.28 \times 10^{2} $ & 0.555\\
		$ 7.32 \times 10^{2} $ & $ 7.31 \times 10^{2} $ & 0.644\\
		$ 7.93 \times 10^{2} $ & $ 7.79 \times 10^{2} $ & 0.758\\
		\hline \hline
	\end{tabular}
\end{table}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{img/gamma-plot.png}
	\caption{A plot of $ \Gamma $ vs $ \sin^{2}( \theta ) $ for the half-micron power spectrum data set.}
	\label{fig:gamma-plot-0.5}
\end{figure}

\indent In the plot and final analysis of the data, the power spectrum data was preferred to the autocorrelation function data because of the ease of fit the model provides. The experimentally determined diameters of the spheres was $ 0.486 \pm .07 \mu$m for the half-micron spheres, and $ 1.217 \pm 0.2 \mu $m spheres for the 1-micron sphres.

\begin{table}[h]
	\centering
	\caption{ $ \Gamma $ vs $ \sin^{2}( \theta ) $ for the 1.0 micron spheres.}
	\label{tab:gamma-tab-1.0}
	\begin{tabular}{ccc}
		$ \Gamma $ from autocorr data & $ \Gamma $ from power spectrum data  & $ \sin^{2} ( \theta ) $ \\
		\hline \hline
		$ 1.34 \times 10^{2} $ & $ 1.34 \times 10^{2} $ & 0.0930 \\
		$ 1.51 \times 10^{2} $ & $ 1.53 \times 10^{2} $ &  0.146 \\
		$ 1.80 \times 10^{2} $ & $ 1.79 \times 10^{2} $ & 0.235 \\
		$ 2.23 \times 10^{2} $ & $ 2.15 \times 10^{2} $ & 0.324 \\
		$ 3.05 \times 10^{2} $ & $ 2.88 \times 10^{2} $ & 0.444 \\
		$ 3.56 \times 10^{2} $ & $ 3.52 \times 10^{2} $ & 0.555 \\
		$ 3.21 \times 10^{2} $ & $ 2.98 \times 10^{2} $ & 0.644 \\
		$ 3.63 \times 10^{2} $ & $ 3.40 \times 10^{2} $ & 0.765 \\
		\hline \hline
	\end{tabular}
\end{table}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{img/gamma-plot-1.png}
	\caption{ $ \Gamma $ versus $ \sin^{2} ( \theta ) $ for the 1 micron spheres.}
	\label{fig:gamma-plot-1.0}
\end{figure}


\section{Conclusions}

\indent The data shows reasonable agreement with both theory and manufacturer data. The theory expected a linear trend between $ \sin^{2}( \theta) $ and $ \Gamma $, which shows agreement in Figure~\ref{fig:gamma-plot-0.5}. Our data was fit without forcing though the origin to use the calculated intercept as a gauge of data quality; a large intercept relative to the data would indicate a significant disagreement between theory and experiment. The intercepts on both sets of data had $ y $-intercepts $ b $ and slopes $ m $ with ratios $ \frac{b}{m} < 10 ms^{-1} $, which will not incur any significant change on the autocorrelation function or the power spectrum fit.
\indent Overall, the data showed agreement with theory and manufacturer data. Data, and stronger agreement, would have come with a more in-depth statistical analysis and overall better treatment of the spheres, including better declumping practices to avoid settling.

\bibliography{DLS}


\appendix























\end{document}

