\documentclass[notitlepage]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{mathrsfs}
\title{Lie Theory in Classical Mechanics and Noether's Theorem}
\author{Samuel Wallace}
\begin{document}
\maketitle

\indent These notes are stand-alone, about applications of Lie groups in classical physics, especially Noether's theorem. The reader should have an understanding of Lagrangian Mechanics before starting this, and be familiar with matrix operations (\textit{Principles of Linear Algebra} for the uninitiated). New topics to be introduced will be matrix groups.

\section{Invariant Lagrangians}
\indent Conserved quantities are very important in physics; they allow us to solve problems easier sometimes. For example, in mechanics problems, conservation of momentum and energy make solving usually intractable problems an easy task. So we'd like a way to systematically produce conserved quantities. The best way to do this is through \textbf{Noether's Theorem}, which gives a way of turning a symmetry of a system into a conserved quantity. Before we get to that, we'll explore the math behind the theorem, and get a better feeling for why it works. \\
\indent First we start off with an example about harmonic oscillators.
\subsection{Harmonic Oscillator}
\indent Consider the lagrangian of a 2-d harmonic oscillator
$$L = \frac{1}{2}m(\dot{x}^2 + \dot{y}^2) - \frac{1}{2}k(x^2 + y^2)$$
An interesting thing about this system is that if we rotate our coordinate frame, our equations of motion don't change. In equations, if we have a matrix $R$ representing a rotation, then 
$$L(Rx,Ry,R\dot{x}, R \dot{y}) = L(x,y,\dot{x},\dot{y})$$
To see this clearly, let's write the above Lagrangian in terms of a vector: $\Vec{v} = (\dot{x}, \dot{y}); \hspace{6pt} \Vec{x} = (x,y)$ So that the Lagrangian becomes 
$$L_R = \frac{1}{2}m R \Vec{v} \cdot R \Vec{v} -\frac{1}{2}k (R \Vec{x}) \cdot (R \Vec{x})$$
Where we have used the fact that $\frac{d}{dt}(R \Vec{x}) = R \dot{\Vec{v}}$. We can rewrite the dot products in the Lagrangian using the transpose as well, so that $(R \Vec{x}) \cdot (R \Vec{x}) = \Vec{x} \cdot (R^T R \Vec{x})$. So for the Lagrangian to remain unchanged, we see that we have to have $\Vec{x}\cdot (R^T R\Vec{x}) = \Vec{x} \cdot \Vec{x}$, which means that $R^T R$ has to be the identity matrix. \\
\indent So now we've done some work and \textit{not} changed our Lagrangian. But you're probably wondering (or not), how many rotation matrices are there? Well, looking back at the original system, we were rotating around the $x-y$ plane. So there should be a rotation for every angle we could rotate through, from $0$ to $2 \pi$. In fact, we could parametrize our rotation matrices by the angle. Let's called the rotation matrix corresponding to a counter-clockwise rotation by the angle $\theta$ $R_{\theta}$. Then we have a whole family of Lagrangians for each angle $\theta$ that shouldn't change. So let's put that into equations:
$$
L_{\theta} = \frac{1}{2}m \Vec{v} \cdot (R^T_{\theta} R_{\theta} \Vec{v}) - \frac{1}{2}k \Vec{x} \cdot (R^T_{\theta} R_{\theta} \Vec{x})
$$
This shouldn't change our Lagrangian, so
$$0=\frac{dL_{\theta}}{d\theta} = \frac{1}{2}m \Vec{v} \cdot \frac{d}{d\theta}( R^T_{\theta} R_{\theta} \Vec{v}) - \frac{1}{2}k \Vec{x} \cdot \frac{d}{d\theta}(R^T_{\theta} R_{\theta} \Vec{x})$$
Now if we use the Leibniz rule on the product of matrices (it works out, trust me! Or don't, and work it out for yourself):
$$\frac{d}{d\theta}(R^T_{\theta} R_{\theta}) = \left( \frac{d}{d\theta} R^T_{\theta} \right) R_{\theta} + R^T_{\theta} \left( \frac{d}{d\theta} R_{\theta} \right)$$
Now let's make some observations. $R_0 = I$, the identity matrix. And if we have some non-identity rotation matrix, we can rotate back to our original coordinate system before taking the derivative. So we can just look at the derivative of the rotation matrix at the identity. Doing that, the derivative we want is 
$$\frac{d}{d\theta}(R^T_{\theta} R_{\theta})\vert_{\theta=0} = \left( \frac{d}{d\theta} R^T_{\theta} \right) \restriction_{\theta=0} + \left( \frac{d}{d\theta} R_{\theta} \right) \restriction_{\theta=0}$$
And I'll let you check that the derivative of the transpose is the transpose of the derivative. Let's write our derivative in the above expression as $r$, so that 
$$
\frac{dL_{\theta}}{d\theta} \restriction_{\theta=0} = \frac{1}{2}m \Vec{v} \cdot ((r^T + r) \Vec{v}) - \frac{1}{2}k \Vec{x} \cdot ((r^T + r)\Vec{x})
$$
Now since this should be zero, we have that this should be zero, we should have that $r^T+r=0$, in other words, $r$ is a skew-symmetric matrix. So the derivative of a rotation matrix is a skew-symmetric matrix. Cool. Now what?\\
\indent Let's take a look at the quantity $\Vec{v}\cdot (r \Vec{x})$ for solution to the Euler-Lagrange equations. Let's take a time derivative:
$$
\frac{d}{dt}(m\Vec{v}\cdot (r \Vec{x})) = m\dot{\vec{v}} \cdot r \vec{x} + m\vec{v} \cdot r \vec{v} = - k\vec{x} \cdot r\vec{x} + m \vec{v} \cdot r\vec{v}
$$
Now since $r$ is skew symmetric, we have that $\vec{x} \cdot r\vec{x} = r^T \vec{x} \cdot \vec{x} = - r \vec{x} \cdot \vec{x} = -\vec{x} \cdot r \vec{x}$, so the dot product is zero. Applying the same to $\vec{v}$, we have that both terms are zero. That means the original quantity is constant! Let's take a closer look at what the quantity was. $r$ is a skew symmetric 2 $\times$ 2 matrix, so it has one independent quantity (it looks like 0's on the diagonal, and the last two entries are negatives of each other). So calculating out:
$$m\vec{v} \cdot r \vec{x} = m \begin{bmatrix} v_1 \\ v_2 \end{bmatrix} \cdot \begin{bmatrix} 0 & a \\ -a & 0 \end{bmatrix} \begin{bmatrix} x_1 \\ x_2 \end{bmatrix}
= ma(v_1 x_2 - x_1 v_2)
$$
Does this look familiar? It's the only nonzero component of the angular momentum! So we've somehow recovered conservation of angular momentum from an quantity derived from a symmetry, $r$. How did that happen?\\
\subsection{Continuous Transformations}
\indent A key component of the angular momentum example was that we were able to identify a way of transforming our coordinate system in a way that didn't cause a change in the Lagrangian. Let's take a look at these typse of tranformations in a little more detail. \\
\indent Usually we have a space that our particles live in; usually this is a plane or 3-d space. Mathematically, these are the vector space $\mathbb{R}^2$ and $\mathbb{R}^3$. However, we can have more unusual places our particle explore. For example, a simple pendulum has a mass that stays on the edge of a circle, and if we have a 'spherical pendulum' (a pendulum that can swing in any direction in 3 dimensions), the end of the pendulum stays on the surface of a sphere. The mathematical name for the circle and sphere are $S^1$ and $S^2$ (note that the number up top is not an exponent, it instead lists the degrees of freedom that a particle in this system can move through). And for more exotic systems, we can imagine stranger 'configuration spaces' might arise. \\
\indent We are going to get conserved quantities when we find 'one-parameter transformations of our configuration space.' To explain what I mean, let's put name some mathematical objects. Let's say our particles live in the space $M$, where $M$ could be the examples we discussed above or something else more strange. Let's also say we have an a function from $M$ to $M$ that depends on a parameter $s$. So if we have a point in our space $x$ that represents the position of a particle, we have a path depending on $s$, $\phi_s x$, where $\phi_s$ is our $s$-dependent map from $M$ to $M$. Let's look at a couple of examples and see if we can make sense of this definition.\\
\indent Let's look back at the harmonic oscillator. Here our map $\phi_s = R_s$. How is this? Where for each $s$, $R_s$ is a map from our space to itself, that is, $R\vec{x} = \vec{y}$, where $\vec{x}$ and $\vec{y}$ are both 2-d vectors. Furthermore $R_s \vec{x}$ is a continuous path when we vary $s$, so it is indeed a 'one-parameter' (depends on $s$) 'transformation of configuration space' (maps 2-d vectors to 2-d vectors). Another example is rotating a sphere by an angle of $s$ radians around an axis. You can check that this is indeed the type of transformation we want.\\
\indent There is also a simplification we can make for calculations to come out a little nicer. We can make $\phi_0$ the identity transformation, that is, the transformation that leaves every point where it started. We'll see later how that becomes useful.
\section{Noether's Theorem}
\indent So how do we go from a continuous transformation to a conserved quantity? This is the content of \textbf{Noether's Theorem}. When I did it in the example, I produced angular moentum out of a hat, with no derivation for how that quantity comes about. Now we'll go through the derivation and see why it works.\\
\indent To start with, we'll need two things: a Lagrangian for a system, and a one-parameter family of transformations that keep the Lagrangian the same as we vary the parameter of our transformations. Let's call our transformations $\phi_s$ and our Lagrangian $L$, as usual, so that the condition of the transformations doesn't affect the Lagrangian becomes:
$$
L(\dot{\vec{x}}, {\vec{x}},t) = L\left(\frac{d}{dt}(\phi_s \vec{x}), \phi_s \vec{x}\right)
$$
Let's evaluate that derivative in the RHS. In general, $\phi_s$ might depend on $\vec{x}$, so we have to use the chain rule to evaluate the derivative:
$$
\frac{d}{dt}(\phi_s \vec{x}) = \frac{\partial (\phi_s \vec{x})}{\partial \vec{x}} \dot{\vec{x}}
$$
Now let's take a look at the action as a function of $s$. Here we'll use the letter $I$ for the action to avoid confusion that we might have if we used the regular $S$. We have that:
$$I_s = \int_{t_0}^{t_1} L\left( \frac{\partial (\phi_s \vec{x})}{\partial \vec{x}} \dot{\vec{x}}, \phi_s \vec{x} \right) dt$$
Now since our Lagrangian didn't change, neither should our action. So we can take a derivative with respect to $s$ and set the expression equal to zero. We'll make the work a little easier by taking the derivative at $s=0$ and using our condition that $\phi_0$ is the identity transformation. 
$$
0=\frac{dI_s}{ds} = \int_{t_0}^{t_1} \frac{d}{ds} L\left( \frac{\partial (\phi_s \vec{x})}{\partial \vec{x}} \dot{\vec{x}}, \phi_s \vec{x} \right) dt
$$

$$
= \int_{t_0}^{t_1} \frac{\partial L}{\partial \dot{\vec{x}}} \frac{d}{ds} \left( \frac{\partial (\phi_s \vec{x})}{\partial \vec{x}} \dot{\vec{x}} \right) + \frac{\partial L}{\partial \vec{x}} \frac{d}{ds} (\phi_s \vec{x}) dt
$$

$$
= \int_{t_0}^{t_1} \frac{\partial L}{\partial \dot{\vec{x}}} \frac{\partial^2 (\phi_s \vec{x})}{\partial \vec{x}\partial s} \dot{\vec{x}} + \frac{\partial L}{\partial \vec{x}} \frac{\partial (\phi_s \vec{x})}{\partial s} dt
$$
Now we'll assume that we are evaluating the action for the real trajectory of the system, i.e. for a solution for the Euler Lagrange equations. Then we'll have in the second term:
$$
\frac{\partial L}{\partial \vec{x}} \frac{\partial (\phi_s \vec{x})}{\partial s} = \frac{d}{dt} \left( \frac{\partial L}{\partial \dot{\vec{x}}} \right) \frac{\partial (\phi_s \vec{x})}{\partial s}
$$
Then we can realize that the term in the the integral is just:
$$\frac{d}{dt} \left( \frac{\partial L}{\partial \dot{\vec{x}}} \frac{\partial (\phi_s \vec{x})}{\partial s} \right)$$
So that when we evaluate the integral:
$$0 = \int_{t_0}^{t_1} \frac{d}{dt} \left( \frac{\partial L}{\partial \dot{\vec{x}}} \frac{\partial (\phi_s \vec{x})}{\partial s} \right) dt = \Delta \left( \frac{\partial L}{\partial \dot{\vec{x}}} \frac{\partial (\phi_s \vec{x})}{\partial s} \right)$$
So we can see the quantity on the RHS doesn't chagne over the time interval. Since the time interval was arbitrary, we can conclude that it holds for all time intervals, i.e. it's a conserved quantity.\\
\indent Next we'll apply this to a slightly more complicated system and see how we can make our conserved quantities more interesting.
\section{Several parameter transformations}
\subsection{Angular Momentum in Three Dimensions}
\indent Let's consider angular momentum in three dimensions and see how (if at all) it's different from two dimensions. Again, our Lagrangian is 
$$L = \frac{1}{2}m \dot{\vec{x}} \cdot \dot{\vec{x}} - \frac{1}{2}k \vec{x} \cdot \vec{x}$$
Again, we have that matrices that satisfy $R^T R = I$ will preserve our Lagrangian. However, these are $3 \times 3$ matrices now, and there are 'more' matrices in $3 \times 3$ that satisfy this. To visualize, we can imagine the rotations. Every rotation is around an axis by an angle. So we can form a vector corresponding to a rotation by letting the direction of the vector be along the axis, and the magnitude calculated as follows: take the angle you rotate through and put in radians between $\-pi$ and $\pi$, then divide it by $\pi$. This will give a number between $-1$ and $1$. Now we can parametrize any rotation by a vector inside the sphere of radius $1$. We'll call the vector corresponding to a rotation the \textbf{rotation vector} of the rotation matrix. \\
\indent Now we have a way of identifying the sphere with rotations. Except this sphere is weird. Think about increasing the magnitude of a vector and looking at the corresponding rotation, we know this means we increase the angle we go through. When we get halfway through a rotation however, our angle changes from just above $\pi$ radians to $-\pi$ radians, so that our magnitude of the vector reverses! That means if we think about our vector in our sphere, it travels our from the center out to the edge of the sphere, and then suddenly reverses as we pass through the outside of the sphere. That's strange. Let's just live with it for now and see how it affects our calculations. \\
\indent Now we'll look at the conserved quantity. We identify one-parameter transformation $\phi_s = R_{s\vec{u}}$, where the RHS is the rotation matrix corresponding to the rotation vector $s \vec{u}$. Now we evaluate the conserved quantity:
$$
\frac{\partial L}{\partial \dot{\vec{x}}} \frac{\partial (\phi_s \vec{x})}{\partial s} = m \vec{v}\cdot \frac{\partial}{\partial s} (R_{s \vec{u}}) \vec{x}
$$
Where we evaluate the derivative as $s=0$. Without actually writing out the matrix, we can find out what the derivative is:
$$
0 = \frac{d}{ds}R^T_{s\vec{u}} R_{s \vec{u}} = r_{\vec{u}}^T + r_{\vec{u}}
$$
Where the derivative is 0 because $R^T_{s\vec{u}} R_{s \vec{u}} = I$ for all $s$. So again, the derivative at 0 is a skew-symmetric matrix. So the conserved quantity is 
$$m \vec{v} \cdot r_{\vec{u}} \vec{x}$$
Let's try to figure our what the matrix $r_{\vec{u}}$ looks like. It's a skew symmetric $3 \times 3$ matrix, so the diagonal is all zero and the remaining 6 entries have to be anti-symmetric, so pairs of them will be negatives, so that there are 6/2=3 independent entries in the matrix. Let's write out the dot product above:
$$m \begin{bmatrix} v_1 \\ v_2 \\v_3 \end{bmatrix} \cdot \begin{bmatrix} 0 & a_3 & a_2 \\ - a_3 & 0 & a_1 \\ -a_2 & a_1 & 0 \end{bmatrix} \begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix} = m \begin{bmatrix} v_1 \\ v_2 \\v_3 \end{bmatrix} \cdot \begin{bmatrix} a_3 x_2 + a_2 x_3 \\ -a_3 x_1 + a_1 x_3 \\ -a_2 x_1 + x_2 a_1 \end{bmatrix}$$
And the rightmost vector above is of the form of something familiar: it's a cross product with some vector $\vec{a}$. We can actually do a little bit better: we can actually determine the vector $a$ up to a scale factor. Remember that $\vec{u}$ is along the axis that the rotation $R_{\vec{u}}$ rotates around; that means the rotation does not change the vector $u$ no matter how much we rotate through:
$$R_{s \vec{u}}\vec{u} = \vec{u} \Rightarrow \frac{d}{ds} R_{s \vec{u}}\vec{u} = 0 = r_{\vec{u}} \vec{u} = \vec{a}\times \vec{u} $$
Since the cross product is zero, that means $\vec{a}$ and $\vec{u}$ are parallel, so that $\vec{a} = c \vec{u}$. Plugging this back into our conserved quantity,
$$m \vec{v} \cdot r_{\vec{u}} \vec{x} = m \vec{v} \cdot ( c\vec{u} \times \vec{x})$$
Using a vector identity, we can rewrite the above as 
$$m \vec{v} \cdot ( c\vec{u} \times \vec{x}) = mc \vec{u} \cdot (\vec{x} \times \vec{v})$$
Note that this is conserved for any vector $u$, so we can reconstruct the vector $m\vec{x} \times \vec{v}$ by taking linear combinations of basis vectors with various dot products for different choices of vector $\vec{u}$. That means the \textit{vector} quantity $ m\vec{v} \times \vec{x}$ is conserved, and this is angular momentum!
\subsection{Several Parameter Transformations}
\indent Let's say we have a transformation dependent on several parameters $s_1, s_2, \ldots, s_n$. Then we can write the transformation in terms of a 'parameter vector' $\vec{s}=(s_1, s_2, \ldots, s_n)$, so that the transformation is $\phi_{\vec{s}}$. Then when we can have a conserved quantity for each parameter:
$$\frac{\partial L}{\partial \dot{\vec{x}}} \frac{\partial (\phi_{\vec{s}})}{\partial s_i}$$
And we can form a whole vector of conserved quanntities:
$$\frac{\partial L}{\partial \dot{\vec{x}}} \frac{\partial (\phi_{\vec{s}})}{\partial \vec{s}}$$
Also it's easy to check that any linear combination of conserved quantities is also conserved.

\section{Lie Theory}
\indent So we've seen some salient examples of how continuous matrix transformations can give rise to conserved quantities. To expand on this, let's take a look at the types of matrix transformations we've seen so far.
\subsection{Matrix Groups}
\indent There's some universal properties of the matrix transformations that we've seen so far. I'll list them:
\begin{enumerate}
    \item The product of any two transformation matrices is again a transformation matrix of the right type (e.g. product of two rotation matrices is again a rotation matrix).
    \item Every transformation matrix has a transformation matrix that undoes whatever the first matrix did.
    \item The identity matrix is always included in our transformation matrices.
\end{enumerate}
\indent These three properties make the transformation matrices we consider a mathematical object called an \textbf{algebraic group}. Groups are a fundamental idea in math that pop up all over the place. By extension, they will have some applications in physics. Transformation matrices is the biggest application. \\
\indent There's an additional feature of the matrices we've been considering: we can make continuous functions out of them. We've been taking derivatives and considering transformation valued functions. This is the other critical feature of our transformations. We can think of there being a continuum of transformation matrices that we can trace paths through and take derivatives of. In fact, we could realize any set of transformation matrices as a curved surface in some higher-dimensional space, as wild as that sounds. This viewpoint makes sense from a Calculus 3 viewpoint of why we have been taking derivatives and considering the derivatives like vectors. Next we'll take a look at jumping from matrices to their derivatives.
\subsection{Forming a Lie Algebra}
\indent When we took derivatives of our rotations, we got skew-symmetric matrices. Why is that? And are they important? \\
\indent Let's take a look at skew symmetric matrices. There's a natural question: Is every skew-symmetric matrix the derivative of some rotation? Let's take a look at some properties of skew-symmetric matrices:
\begin{enumerate}
    \item The sum of any two skew-symmetric matrices is again skew-symmetric.
    \item Any multiple of a skew-symmetric matrix is skew-symmetric.
\end{enumerate}
This means skew-symmetric matrices form a \textbf{vector space}. Moreover, we can take powers of skew-symmetric matrices. Using these properties, let's see if we can generate a rotation matrix for a given skew-symmetric matrix.\\
\indent Let's look at the derivative of a rotation again. We have that 
$$
0 = \frac{d}{ds}\left( R^T_{s\vec{u}} R_{s \vec{u}} \right) = R^{\prime T}_{s\vec{u}} R_{s\vec{u}} + R^T_{s\vec{u}} R^{\prime}_{s \vec{u}}
$$
\indent Where I am using a prime to denote the derivative of the rotation with respect to $s$. We can solve this for $R^{\prime}_{s \vec{u}}$:
$$R^{\prime}_{s\vec{u}} = - (R_{s\vec{u}}) (R^{T \prime}_{s\vec{u}}) (R_{s \vec{u}})$$
This is a differential equation for a matrix valued function that we can impose initial conditions on. Specifically, we can impose the condition that $R_0 = I$ and $R^{\prime}_0 = q$, for any skew-symmetric matrix $q$. Then by working the derivation of the differential equation backwards, we can say that the resulting matrix equation will definitely be a rotation matrix. Now by standard differential equation theorems, there is a solution for in the interval $(-\varepsilon, \varepsilon)$ for some $\varepsilon > 0$. Now we can say for certain there is a rotation matrix with derivative that is a given skew-symmetric matrix. \\
\indent So what's so important about these skew-symmetric matrices? Let's take a look back at the definition where they came from: $r\vec{x} = \frac{d}{ds}R_{s\vec{u}} \vec{x}$. We can imagine that for a given $\vec{x}$, the function of s $R_{s\vec{u}}\vec{x}$ traces out a path in configuration space. Then $r\vec{x}$ point in the direction that $\vec{x}$ travels as we scale up $s$, it tell us what direction the point $\vec{x}$ will go. We can extend this to a map $\vec{x} \mapsto r\vec{x}$, which defines a vector field on configuration space, pointing in the direction that each point moves. We call this vector field the \textbf{infinitesimal generator} for the transformation.\\
\indent Infinitesimal generators turn out to be very nice, because it's easier to work with vector spaces than with groups. We'll see how to do that in the next section.
\subsection{Lie Group-Lie Algebra Correspondence}
\indent So we know that there are these matrix groups we can work with, and their derivatives creating infinitesimal generators with a very visual interpretation. But what's the convenient way to go between them? Solving differential equations doesn't work very well practically, because often we will be solving many differential equation simultaneously, which is a pain. However, there is a 'canonical' way of going between the two called the exponential map. \\
\indent Let's try to come up with a function that will conveniently let us run between Lie groups and Lie algebras. Usually Lie groups are denoted by $G$ and the corresponding space of derivative matrices by $\mathfrak{g}$, and elements of the group are denoted by lower-case letters and the derivatives by uppercase letters. We want some nice features of this function $f$ to make it useful:
\begin{enumerate}
    \item $f(X+Y) = f(X)f(Y)$
    \item $f(\alpha X) = f(X)^{\alpha}$
    \item $\frac{d}{dt} f(tX) = X f(tX)$
\end{enumerate}
\indent We like these properties because they make transferring from derivatives to group elements easy, and keep the derivatives as derivatives proper. \\
\indent From the first property we can immediately say that $f(X) = f(X+0) = f(X)f(0)$, so that $f(0)$ has to be the identity matrix. Let's calculate a Taylor series around the identity matrix:
$$f(tX) = I + \frac{d}{dt}f(tX) + \frac{1}{2}t^2 \frac{d^2}{dt^2}f(tX) + \ldots$$
$$=I + X + \frac{1}{2}t^2X^2 + \ldots$$
\indent Where I recovered the second derivative by differentiating property 3 again. This may look familiar to you, it looks like the exponential function, but for matrices! This map is actually called the exponential map in Lie Theory. It's one of the fundamental tools in Lie theory, it allows us to pass seamlessly between groups and the derivatives. The formal definition of the exponential map is:
$$\mathrm{exp}(X) = I + X + X^2/2 + \ldots + X^n/n! + \ldots$$
\indent You can check that it satisfies the condition I listed above. The exponential map is the most 'natural' map between Lie groups and their derivatives, in that it defines paths through the Lie group by the function $g_X(t) = \mathrm{exp}(tX)$, and these, by property 3, automatically satisfy $\frac{d}{dt}g_X(t) \restriction_{t=0} = X$. So now coming up with one-parameter transformations is really easy, you can use the exponential map.
\subsection{Non-Abelian Groups}
\indent Now that we have the exponential map at hand, let's use it to analyze some Lie groups by their derivatives. Let's take the 3-d rotations again, and notice something interesting about them: if you take two rotations and do one after the other, the result is another rotation, but you can get different results depending on which order you do it in! You can see this visually by thinking about two rotations: imagine a 90 degree rotation first around the x-axis, then around the y-axis. Draw some spheres on a piece of paper and you'll see it for yourself.\\
\indent So let's try to get a feel for the difference between two different rotations. There's a good way to measure the difference between these two rotations, it's called the \textbf{group commutator}. This is an expression of the form:
$$R^T_{\vec{u}}R^T_{\vec{v}} R_{\vec{u}}R_{\vec{v}}$$
If this expression happens to be the identity matrix, then the two rotations can be done in either order and the result is the same. However, if they are not, this is the result of doing one rotation, then the other, and then undoing them in opposite order. We can think that the 'smaller' the resulting matrix is, the 'more' interchangeable the two operations are. If the commutator of two rotations is the identity, we'll say the rotations \textbf{commute}. Let's see what this means for derivatives.\\
\indent The rotations can be expressed as the exponential of a skew-symmetric matrix. Let's see the infinitesimal generator of a group commutator:
$$
\frac{\partial^2}{\partial s \partial t}\left( \mathrm{exp}(t r_{\vec{u}}) \mathrm{exp}(s r_{\vec{v}}) \mathrm{exp}(-t r_{\vec{u}}) \mathrm{exp}(-s r_{\vec{v}})\right) \restriction_{(s,t)=(0,0)}
$$
This evaluates to:
$$r_{\vec{u}}r_{\vec{v}} - r_{\vec{v}}r_{\vec{u}}$$
Since this is the derivative of a rotation, it is again skew-symmetric (you can check this by matrix multiplication too if you want). This expression has a special name, it's the \textbf{matrix commutator} or \textbf{Lie bracket}, and it's often written as $[X,Y]$. It's an operation that combines our derivatives and gives us geometric information on the rotations. Notice that the Lie bracket of two derivatives vanish exactly when the rotations are order-insensitive. \\
\indent With a Lie bracket, we can figure out through matrix multiplication if two rotations commute. This is an important structure to have on our vector space of derivatives. Let's list off some properties that I won't bother to prove:
\begin{enumerate}
    \item $[X,Y]=-[Y,X]$
    \item $[aX+bY,Z]=a[X,Z] + b [Y,Z]$
    \item $[X,[Y,Z]]+[Z,[X,Y]]+[Y,[Z,X]]=0$
\end{enumerate}
The last of these is called the \textbf{Jacobi Identity}. Lie brackets are incredibly important in quantum mechanics where you deal with infinite dimensional Lie groups, which also show up in fluid dynamics. Now let's go back to angular momentum and see how all this new theory helps us out.


\section{Angular Momentum Revisited}
\indent Let's take a look at the Lie Group and Lie algebra we deal with in angular momentum problems. The group is matrices satisfying $R^TR=I$, and the Lie algebra is skew-symmetric matrices; $r^T + r = 0$. The Lie bracket is the matrix commutator. So let's take a look at the Lie bracket.\\
\indent For a given skew-symmetric matrix $r$, the map $\mathrm{br}_r; \hspace{4pt} \mathrm{br}_r(s) = [r,s]$ is a linear map. Let's analyze it as a linear map. More specifically, let's look at its null space. This would be the matrices $s$ such that $[r,s]=0$. What this means practically is that the rotation matrix $\mathrm{exp}(s)$ commutes with $\mathrm{exp}(r)$. We've seen what the action of a skew-symmetric matrix is on a vector; it looks like a cross product. Let's see if we use that to figure out what the null space is.\\
\indent First off, let's evaluate the bracket on a vector and simplify. 
$$
[r_{\vec{u}},r_{\vec{v}}]\vec{x}=r_{\vec{u}} r_{\vec{v}}\vec{x}- r_{\vec{v}} r_{\vec{u}} \vec{x} = \vec{u} \times ( \vec{v} \times \vec{x}) - \vec{v} \times (\vec{u} \times \vec{x}) = 2 (\vec{u} \times \vec{v}) \times \vec{x}$$
$$= 2 r_{\vec{u} \times \vec{v}} \vec{x}$$
So the Lie bracket is a cross product! That means if we want to solve $[r_{\vec{u}}, r_{\vec{v}}]=0$, we have to solve $\vec{u} \times \vec{v}=0$, which is considerably simpler. It tells us that the null space has dimension one; it's a line in space, and is the line passing through $\vec{u}$. This tells us that the only rotations that commute are the ones through the same axis.\\
\indent Now let's consider some rotations that don't commute. We can consider $\mathrm{exp}(t [X,Y])$ to see where the difference in the rotations leads us (do you have a good interpretation of what this expression means?). It's a rotation about the axis that the vector $\vec{u} \times \vec{v}$ lies in, meaning it's perpendicular to both the rotation axes of the two rotations. This means we can realize commutator of two rotations as a rotation around a vector perpendicular to the axes of the rotations. \\
\indent Now that we know that the angular momentum is constant, we can use it reduce our equations of motion. Since the angular momentum vector is constant, we have a relation between position and momentum that we can use to reduce the equations of motion. Let's make a rotation of our coordinate system so that angular momentum lines up with the $z$ axis. Because the angular momentum vector doesn't change, we don't need a transformation that changes in time, but only a fixed one. So let's say $\vec{x}(t)$ is a solution to the Euler Lagrange equations, then there is a rotation matrix $\Omega$ so that $m \vec{v} \times \Omega \vec{x} \propto \widehat{z}$. Now because $\vec{x}$ and $\vec{v}$ are perpendicular to the angular momentum, they lie in the $x-y$ plane. So now we have two equations defining motion, $x(t), y(t)$ rather than all three variables. Let's look at the Lagrangian in the reduced variables:
$$L= \frac{1}{2}m(\Omega\dot{\vec{x}}\cdot \Omega \dot{\vec{x}}(t) - \frac{1}{2}k(\Omega\vec{x}(t)\cdot \Omega \vec{x}(t))$$
$$=\frac{1}{2}m(\dot{x}(t) + \dot{y}(t)) - \frac{1}{2}k(x(t)^2 + y(t)^2)$$
\indent Because $\Omega \vec{x}$ has no $z$ component. Now we only have a Lagrangian of two variables, one pesky equation eliminated! Now we could solve the system in terms of these new variables, which is considerably simpler. \\
\indent There's a lot more I could with this example, but I think I'll leave the interesting stuff for another time.




\end{document}