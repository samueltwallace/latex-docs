\documentclass{report}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{amsmath}

\begin{document}
\begin{center}
\Large \textbf{Introduction to Operator Theory}\\
\small Samuel Wallace\\
\end{center}
These notes are meant as a follow-up to Principles of Linear Algebra. This set of notes will discuss the additional structures on an infinite dimensional vector space, and their consequences on linear operators on the space. This is meant as a prelude to an abstract understanding of differential equations.
\section{Infinite Dimensional Vector Spaces}
\indent \indent There are many problems with infinite dimensional vector spaces. Everything we did in finite-dimensional vector spaces was made much easier by the existence of bases, where we can reduce our abstract vectors to lists of numbers with reference to a fixed basis. Now, in infinite dimensions, we have no basis. What are we to do without a basis?\\
\indent As I alluded to in \textit{Principles of Linear Algebra}, there are a couple of fixes. The first is the creation of a norm function. This function takes a vector and assigns it a length, in a nearly arbitrary way. We write the norm of a vector $v$ as $\Vert v \Vert$. Our norm function has to follow several simple rules to qualify as a bona fide norm:
\begin{enumerate}
\item The norm of a non-zero vector is greater than 0
\item The norm of the zero vector is exactly zero
\item $\Vert v + w \Vert \leq  \Vert v \Vert + \Vert w \Vert$
\item $\Vert \alpha v \Vert = \vert \alpha \vert \Vert v \Vert$
\end{enumerate}
Where $\alpha$ is a real or complex number. If all these rules are satisfied by a function, then it is a norm. A vector space with a norm is a Banach Space. In Banach spaces, we can form a "basis" by considering partial sums of vectors from an infinite set. Consider an infinite set of vectors $\{v_1, v_2, v_3, \ldots, \}$. We can think of "infinite linear combinations" of these vectors, which are infinite sums of the form:
\[\sum_{j=1}^{\infty} a_j v_j\]
But at this point, this is mathematically nonsense, how can we add up an infinite amount of vectors? Our interpretation of this infinite sum is to find a vector $v$ such that:
\[\lim_{n \to \infty} \Vert v - \sum_{j=1}^{n} a_j v_j \Vert = 0\]
If every vector in our space can be expressed as an "infinite linear combination" of our set of vectors, then we call it a basis for the Banach space (more specifically, this is a type of basis called a Schauder Basis). \\
\textbf{Examples}
\begin{enumerate}
\item A finite-dimensional vector space forms a Banach space if it has a metric tensor, and the norm of a vector is $\sqrt{g_{ij} v^i v^j}$. In this case, an "infinite linear combination" of vectors can be formed by taking every vector beyond the chosen basis to be the zero vector.
\item Consider the space of simple sequences, that is, infinite lists of numbers $(a_1, a_2, \ldots, )$ where addition is in each component, scalar multiplication is across all entries, and there are not infinitely many nonzero numbers in the sequence. Then impose the norm $\sqrt{a_1^2 + a_2^2 + \ldots}$. This forms a Banach Space. It has a standard basis of $\{(1,0,\ldots), (0,1,\ldots), \ldots\}$.
\item One of the most common Banach spaces in analysis is bounded functions on an interval. Consider $C^0 [a,b]=\{f:[a,b] \to \mathbb{R}: f \hspace{2pt} \mathrm{is} \hspace{2pt} \mathrm{continuous} \}$. Since continuous functions are bounded when $a,b$ are finite, we can impose the norm $\Vert f \Vert = \sup_{x \in [a,b]} \vert f(x) \vert$. This turns the space of continuous functions into a Banach space. The Stone-Weierstrass theorem states that polynomials form a basis, $\{1,x,x^2, \ldots\}$ (You should be slightly amazed by this fact, it's pretty incredible that we can use polynomials to approximate any continuous function to any desired accuracy!).
\end{enumerate}
\section{Operators}
\indent \indent Now the most important part of Banach space: Operators. An operator on a Banach space is a function from the space to itself that is linear: $A(x+y) = A(x) + A(y)$, and we often write the action of an operator without parentheses: $Ax$. Any way of modifying an operator that is linear qualifies as an operator.\\
\textbf{Examples}
\begin{enumerate}
\item The "shift operator": $(Af)(x) = f(x+1)$, acting on $C^0[a,b]$.
\item The square operator: $(Af)(x) = [f(x)]^2$ is NOT linear.
\item The derivative, acting on $C^{\infty}[a,b]$ with the same norm as $C^0[a,b]$.
\end{enumerate}
\indent \indent In finite dimensions, we have nice properties of linear operators. We could characterize them as tensors and had a similar vector space for linear operators. But in infinite dimensions, operators are a little 'wilder:' we don't have as many nice properties. To show this, let's examine one of the basic operators from calculus: the derivative. We know that, by linearity, the derivative of the zero function is itself. However, if we have a sequence of functions that converge to zero, the derivative may not converge to zero! The classic example of this is:
\[s_n(x)= \frac{1}{n}\sin(n^2 x)\]
\[\Rightarrow s_n'(x) = n \cos(n^2 x)\]
So the function converges to zero, but its derivative diverges! This is a strange phenomenon. Because of the linearity of this operator, we can change any sequence to not converge in this way! Let's play the following trick. Let's say $f_n \to f$ (the sequence $f_n$ converges to $f$), and $f_n'$ converges to something. Then we create a new sequence $f_n + s_n$, since $s_n$ converges to zero, the sequence still converges to $f$, but now the sequence of derivatives may not converge, since the sequence looks like $f_n'+s_n'$, and $s_n'$ diverges. This means that if we want to represent a function as a sequence, we can't necessarily understand the behavior of the sequence through its derivatives. This makes relatively ordinary operators like the derivative significantly harder to work with, and will make machinery we learn later harder to work with.\\
\indent \indent We call operators which take some convergent sequences to divergent sequences \textbf{discontinuous} operators. Operators which keep convergent series as convergent are \textbf{continuous} operators. I'm not going to prove it, but an equivalent condition to being continuous is that an operators is \textbf{bounded}, which in equation form is an operator that satisfies $\Vert Ax \Vert \leq C \Vert x \Vert$ for some constant $C$. Since being bounded and being continuous are equivalent, you should instantly think of the other when you hear one. Sometimes it's easier to show that an operator is bounded rather than continuous, and sometimes the other way around, so it's necessary to be familiar with both definition.\\
\indent Inverse operators retain many of the abstract properties that inverse of linear transformations have. First of all, to make a sensible definition of an inverse, we require $A^{-1} A v = A A^{-1} v = v$ for all vectors $v$. This means if we try to solve an equation like $Av=w$ for $v$, we should only get one answer. By linearity, we can say that finding one solution to an arbitrary equation is the same as finding a single solution for $Av=0$. So if we find one solution to the "homogeneous equation," $Av=0$, then an inverse exists. This inverse won't be as easy to find as in the finite dimensional case, but it exists!\\
\indent An operator is \textbf{bounded away from zero} if $\Vert Ax \Vert \geq C \Vert x \Vert$ for some constant $C$. An operator is bounded away from zero if and only if the operator has a bounded inverse.
\section{Hilbert Spaces}
\indent \indent Now we're going to introduce a finer structure on a vector space to give it more to work with in terms of operators. We introduce an \textbf{inner product} on a vector space. This is a function that behaves much like a metric tensor in finite dimensions, now allowed to extend to infinite dimensions. An inner product between two vectors, $v$ and $w$ is a number $<v,w>$ that obeys the following rules:
\begin{enumerate}
\item $<v,w> = \overline{<w,v>}$ (complex conjugation)
\item $<z+v, w> = <z,w> + <v,w>$
\item $<v,v> = 0 \Rightarrow v = 0$
\item $<v,v> \geq 0$
\end{enumerate}
It follows from these conditions that the number $\sqrt{<v,v,>}$ is a norm. Thus, every Hilbert space is a Banach space (this is why I called it a finer structure, it gives more information than just a norm does). The interpretation of an inner product is that of an angle: it gives the angle between two vectors (more specifically the inner product of two vectors is proportional to the cosine of the angle between them).\\
\textbf{Examples}
\begin{enumerate}
\item Every Riemannian metric gives rise to a Hilbert space.
\item Square summable sequences form a Hilbert space: series for which $a_1^2 + a_2^2 + \ldots$ is finite. The inner product between two sequences $a_i$ and $b_i$ is $a_1 b_1 + a_2 b_2 + \ldots$ (Here the angle interpretation holds slightly less water, it's not clear what we mean by the "angle" between two sequences, but we still value it as a Hilbert space).
\item One of the most important Hilbert spaces is that of square-integrable functions: functions for which $\int \vert f(x) \vert ^2 dx$ is finite. The inner product between functions $f$ and $g$ is $\int f g dx$ (Again, calling this an angle doesn't do much for us, but it's still a useful as a Hilbert Space) This is called the norm \textbf{induced} by the inner product.
\end{enumerate}
There are quite a few useful properties of Hilbert spaces we can develop. A useful one is the \textbf{Cauchy Schwartz Inequality:}
\[ <v,w> \leq \Vert v \Vert \cdot \Vert w \Vert \].\\
\indent We can also do something special with bases. We can develop a special kind of bases called \textbf{orthonormal}. This kind of basis is one in which the basis vectors are \textbf{normalized} and \textbf{orthogonal}. A vector is normalized when its norm is 1. Two vectors are orthogonal when their inner product is zero (this term comes from geometry, two lines are orthogonal when they are 90 degrees apart, which is when the cosine of their angle is zero). So an orthonormal basis is a basis where every vector has norm 1 and every pair of vectors is orthogonal. There are a couple of nice properties of orthonormal bases: if we expand a vector $v$ in terms of an orthonormal basis $\{\varphi_i \}$:
\[v = \sum_{n=1}^{\infty} v_i \varphi_i\]
And we take an inner product with a basis vector $\varphi_j$, then we only get the $j$-th component:
\[<v, \varphi_j> = v_j\]
(It isn't hard to work out why - try it yourself!). An orthonormal basis is sometimes called a \textbf{generalized Fourier Basis}, and an expansion over these basis elements is called a \textbf{generalized Fourier expansion}. The more familiar Fourier series you might see is a special case of this for a specific Hilbert space (think about which one it might be!). \\
\\ Another useful identity is the \textbf{Parseval-Steklov Equality}, which says that if we break down a vector in a generalized Fourier expansion, the following holds:
\[<v,v> = \sum_{i=1}^{\infty} v_i^2\]
Which tells us that the norm of a vector is independent of the Fourier Basis we expand over. Additionally, it looks like a regular vector dot product, extended to infinite dimensions!\\
\indent There's often used bases and inner products for funtion spaces, but there are more exotic but equally useful ones. In addition, it's worth asking how easy it is to find an orthonormal basis, since they have so many nice properties. We will address both of these in the next section.
\section{Graham-Schmidt Orthogonalization and Exotic Inner Products}
\indent \indent Here we will give an algorithm for creating an orthonormal basis from a basis that is not orthonormal. Then we will discuss less common inner products on function spaces and their implication for bases.\\
\indent Let's say we have any old bases, $\{\psi_i\}$. It doesn't have to be orthonormal or anything special at all. It just has to span the Hilbert space we're interesed in. Now let's say we want to make an orthonormal basis out of this first basis somehow, and we want to do this for nice properties like inner products and the Parseval-Steklov equality. We'll do this by a process called \textbf{Gram-Schmidt Orthogonalization}.\\
\indent Gram-Schmidt orthogonalization works like this: We start with our first regular old basis vector, $\psi_1$, and normalize it, which is to say we start a new basis with a first vector
\[\varphi_1 = \frac{\psi_1}{\sqrt{<\psi_1, \psi_1>}}\]
Now we start our second vector in our new orthonormal basis by making it a linear combination of the first two of our old basis vectors, and then solving for the coefficients of the linear combination by demanding the new vector is orthonormal to our first vector:
\[\phi_2 = \psi_1 + a_2 \psi_2\]
\[< \phi_2, \varphi_1 > = 0 \Rightarrow a_2 = - <\psi_2, \varphi_0 >\]
Now we have to normalize this vector: 
\[\varphi_2 = \frac{\phi_2}{\sqrt{< \phi_2, \phi_2>}}\]
We can continue this process indefinitely, giving the following formulas:
\[\phi_1 = \psi_1; \hspace{8pt} \varphi_1 = \frac{\phi_1}{\Vert \phi_1 \Vert}\]
\[\phi_2 = \psi_2 - \frac{<\phi_1, \psi_2>}{<\phi_1, \phi_1>} \phi_1; \hspace{8pt} \varphi_2 = \frac{\phi_2}{\Vert \phi_2 \Vert}\]
\[\phi_3 = \psi_3 - \frac{<\phi_1, \psi_3>}{<\phi_1, \phi_1>} \phi_1 - \frac{<\phi_2, \psi_2>}{<\phi_2, \phi_2>} \phi_2; \hspace{8pt } \varphi_3 = \frac{\phi_3}{\Vert \phi_3 \Vert}\]
\[ \vdots \]
This is guaranteed, by construction, to create an orthonormal basis from any basis (Note that this process is useful only analytically; computationally, it is an inefficient process). Now let's apply this to important Hilbert spaces.\\
\indent One of the most important Hilbert spaces, because of its use in quantum mechanics, is square-integrable functions, which were introduced in a previous example. Let's take the restricted space of functions over the interval $[-1,1]$. We would like to form an orthonormal basis for this function space. We know a basis for this space: monomials, $\{1,x,x^2,\ldots\}$. If we take our inner product of functions to be:
\[<f,g> = \int_{-1}^1 f(x) \overline{g(x)} dx\]
Then we can start Gram-Schmidt orthogonalization. I'll leave the calculation to you, but the results are polynomials:
\[P_0(x) = 1\]
\[P_1(x) = x\]
\[P_2(x) = \frac{1}{2} (3 x^2 - 1)\]
\[P_3(x) = \frac{1}{2} (5x^3 3x)\]
\[\vdots\]
These are the \textbf{Legendre Polynomials}. They show up a lot in solving Laplace's equation in spherical coordinates (very common in electrostatics). \\
\indent Now what if we changed our inner product for this space? Let's instead consider the inner product given by the following:
\[ <f,g> = \int_{-1}^1 f(x) \overline{g(x)} x^2 dx\]
(You should go through the rules for inner products and verify this is still an inner product). Would this change the polynomials we get from Gram-Schmidt orthogonalization? The additional term of $x^2$ in the integral will surely change the value of an inner product compared to our "standard" inner product for this function space, and so when we calculate the inner product in the Gram-Schmidt formulas, we should get different numbers. I'll again leave the computation to you, but if you carry out Gram-Schmidt with the new inner product, you get:
\[\varphi_1 = \sqrt{\frac{3}{2}}\]
\[\varphi_2 = \sqrt{\frac{5}{2}} x\]
\[\varphi_3 = \frac{5}{\sqrt{22-20\sqrt{\frac{2}{3}}}} \left( x^2 - \sqrt{\frac{6}{5}}\right)\]
And these won't get any nicer from here on out, so I'll leave them out. Needless to say, the change in inner product caused a drastic change in the resultsof Gram-Schmidt Orthogonalization!\\
\indent This changing of the inner product can give rise to some commonplace results. Let's take the functions space of square-integrable functions over the whole real line. Let's change our inner product to be:
\[ <f,g> = \int_{-\infty}^{\infty} f(x) \overline{g(x)} e^{-x^2} dx \]
If we do Gram-Schmidt Orthogonalization, the result is well-known:
\[H_1(x) = 1\]
\[H_2(x) = 2x\]
\[H_3(x) = 4x^2 - 2\]
\[\vdots\]
These are the \textbf{Hermite Polynomials}, used in quantum mechanics. The other common polynomials comes from functions on the half-line with the inner product:
\[ <f,g> = \int_0^{\infty} f(x) \overline{g(x)}e^{-x} dx\]
Applying Gram-Schmidt Orthogonalization with this inner product creates the \textbf{Laguerre Polynomials}:
\[L_1(x) = 1\]
\[L_2(x) = 1-x\]
\[L_3(x) = \frac{1}{2} (x^2 - 4x + 2)\]
\[\vdots\]
These also show up in quantum mechanics, in the hydrogen atom. There are many other orthogonal polynomials that appear over different domains and with different inner products, for example, Chebyshev and Jacobi polynomials, but these have less applications in physics than the ones introduced previously, so we won't cover them here.
\section{Operators on Hilbert Spaces}
Operators on Hilbert spaces come with much more richness due to the inner product. The most important construct of an operator in a Hilbert space is the \textbf{Adjoint}. If you have an operator $A$, its adjoint, $A^*$, is an operator such that:
\[ <Au,v> = <u,A^* v>\]
An adjoint tells us what happens if we "flip" an operator onto the other side of an inner product.\\
\textbf{Exercises}
\begin{enumerate}
\item Consider a tensor on a finite dimensional space of rank (1,1), which is to say, a linear transform. What is its adjoint, given an arbitrary Riemmanian metric on the space?
\item Consider the shift operator from a previous example. What is its adjoint?
\item Consider the space $C^{\infty} [a,b]$ with the boundary condition on the space that $f(a)=f(b)$. Now consider the derivative. What is its adjoint?
\end{enumerate}
The next important concept for operators in Hilbert spaces is that of eigenvectors. If we have a linear transformation $A$ on a Hilbert space, it is a vector that satisfies
\[Av = \lambda v\]
Where $\lambda$ is a number. This is like eigevectors we saw in finite dimensional cases, but now we may have infinitely many of them in an infinite-dimensional space. Eigenvalue problems in infinite-dimensional spaces are the core problem of quantum mechanics, so their theory is very important.\\
\indent Often times the relevant Hilbert space is a function space, and the operator is a differential operator, so the eigenvalue problem is a differential equation. Methods for solutions of differential equations will be covered in additional notes.\\
\indent A very important class of operators are those for which they are their own adjoint. Examples include the second derivative, and multiplication by a real-valued function (these are the two central operators of quantum mechanics). Operators that are their own adjoint are called \textbf{self-adjoint}, or sometimes \textbf{Hermitian}. Self-adjoint operators are very well-behaved because of the spectral theorem: \textbf{The eigenfunctions of a self-adjoint operator form a basis}. This is heavily abused in quantum mechanics.\\
\indent To give an example, consider the operator $A = -\frac{d^2}{dx^2} + x^2$. Its eigenvectors are the functions:
\[ H_n e^{-x^2} \]
Where the $H_n$ are the Hermite polynomials. As we saw from the Gram-Schmidt process, these indeed form a basis. Most of the examples here are rather complex, so I won't give many more examples, but if you study quantum mechanics, you will surely see more.







\end{document}
